#include "gmdns.h"
#include "gmdns-service.h"

#include <arpa/inet.h>

#undef DEBUG

struct {
	GmDNSService *service;
	const char *protocol;
	guint       port;
	const char *script;
	const char *path;
} services [] = {
	{ NULL, "http", 80, "httpd", "/" },
	{ NULL, "ssh",  22, "sshd",  NULL },
	{ NULL, "sftp", 22, "sshd",  NULL}
};

static gboolean
service_is_running (guint index) {
	char *path, *cmd;
	gint retval;

	path = g_build_filename ("/etc/init.d", services[index].script, NULL);
	if (!g_file_test (path, G_FILE_TEST_EXISTS)) {
		g_free (path);
		return FALSE;
	}

	cmd = g_strdup_printf ("%s status", path);
	g_free (path);

	if (!g_spawn_command_line_sync (cmd, NULL, NULL, &retval, NULL)) {
#ifdef DEBUG
		g_message ("%s is not running", services[index].script);
#endif
		g_free (cmd);
		return FALSE;
	}

#ifdef DEBUG
	g_message ("%s is %s", services[index].script,
			(retval == 0) ? "running" : "not running");
#endif

	g_free (cmd);
	return (retval == 0);
}

static void
launch_service (guint index) {
	if (service_is_running (index)) {
		if (services[index].service == NULL) {
			GmDNSService *service;

			service = gmdns_service_new ("wyatt",
					services[index].protocol, NULL, NULL);
			gmdns_service_set_address (service,
					inet_addr("192.168.1.5"),
					services[index].port);
//			if (services[index].path) {
				gmdns_service_add_extra (service,
						"path",
						services[index].path);
//			}
			gmdns_service_start (service);

#ifdef DEBUG
			g_message ("started %s", services[index].protocol);
#endif
			services[index].service = service;
		} else {
#ifdef DEBUG
			g_message ("%s already running",
					services[index].protocol);
#endif
		}
	} else {
		if (services[index].service) {
#ifdef DEBUG
			g_message ("stopping %s", services[index].protocol);
#endif
			g_object_unref (services[index].service);
			services[index].service = NULL;
		} else {
#ifdef DEBUG
			g_message ("%s already stopped",
					services[index].protocol);
#endif
		}
	}
}

static void
launch_services_idle (void) {
	guint i;

	for (i = 0; i < G_N_ELEMENTS (services); i++) {
		launch_service (i);
	}
}

static void
launch_services (void) {
	launch_services_idle ();
}

int
main (int argc, char** argv)
{
	GMainContext *context;
	GMainLoop *loop;

	g_type_init ();

	context = g_main_context_default ();
	gmdns_init (context);

	launch_services ();
	g_timeout_add (5000, (GSourceFunc) launch_services_idle, NULL);

	loop = g_main_loop_new (context, FALSE);
	g_main_loop_run (loop);

	return 0;
}
