#include "gmdns.h"
#include "gmdns-service-query.h"

#include <gtk/gtk.h>
#include <arpa/inet.h>
#include <string.h>

enum {
	COLUMN_SERVER,
	COLUMN_IP,
	COLUMN_PORT,
	COLUMN_PATH,
	NUM_COLUMNS
};

static GtkWidget* tree_view = NULL;
static GtkWidget* window = NULL;
static GtkListStore* list_store = NULL;

static void
answer_callback (GmDNSServiceQuery* 		query, 
		 const GmDNSServiceQueryAnswer*	answer, 
		 gpointer 			data) {
	GtkTreeIter iter;
	GValue value = { 0 };
	const char* s;

	if (answer->answer_type == SERVICE_QUERY_ANSWER_FOUND) {
		/* this is a new service - add a row */
		gtk_list_store_append (list_store, &iter);
	} else {
		/* this is an update or a delete - we need to find the old
		 * row. */
		gtk_tree_model_get_iter_first (GTK_TREE_MODEL (list_store), 
				&iter);
		for (;;) {
			gtk_tree_model_get_value (GTK_TREE_MODEL (list_store), 
					&iter, COLUMN_SERVER, &value);
			s = g_value_get_string (&value);
			if (!strcmp (s, answer->name)) {
				g_value_unset (&value);
				break;
			}
			g_value_unset (&value);

			if (!gtk_tree_model_iter_next (
						GTK_TREE_MODEL (list_store), 
						&iter)) {
				g_warning ("failed to find row for \"%s\"",
						answer->name);
				break;
			}
		}
	}

	/* now iter points to the row we care about */

	if (answer->answer_type == SERVICE_QUERY_ANSWER_REMOVED) {
		/* its a remove request - so remove the row */
		gtk_list_store_remove (list_store, &iter);
		return;
	}

	if (answer->answer_type == SERVICE_QUERY_ANSWER_FOUND) {
		/* we get the name when the service is found */
		gtk_list_store_set (list_store, &iter, 
				COLUMN_SERVER, answer->name, -1);
	}

	/* FOUND and UPDATED behave the same from here - based on
	 * answer->changed_fields we have to update columns in the row
	 * pointed to by iter
	 */
	if (answer->changed_fields & SERVICE_QUERY_ANSWER_PORT) {
		g_message ("PORT PORT GOOD FOR CHRISTMAS (%d)", answer->port);
		gtk_list_store_set (list_store, &iter, 
				COLUMN_PORT, answer->port,
				-1);
	}

	if (answer->changed_fields & SERVICE_QUERY_ANSWER_IP) {
		gtk_list_store_set (list_store, &iter, 
				COLUMN_IP, inet_ntoa (answer->ip),
				-1);
	}
	if (answer->changed_fields & SERVICE_QUERY_ANSWER_EXTRA) {
		s = g_hash_table_lookup (answer->extra, "path");
		g_message ("EXTRA EXTRA READ ALL ABOUT IT (%s)", s);
		if (s == NULL) {
			s = "";
		}
		gtk_list_store_set (list_store, &iter, COLUMN_PATH, s, -1);
	}

	g_print ("FOUND!\n");
}

int
main (int argc, char** argv)
{
	GmDNSServiceQuery *query;

	/* initialise */
	gtk_init (&argc, &argv);
	gmdns_init (g_main_context_default ());

	/* setup up list store */
	list_store = gtk_list_store_new (NUM_COLUMNS, G_TYPE_STRING, 
			G_TYPE_STRING, G_TYPE_UINT, G_TYPE_STRING);

	/* set up gui */
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	tree_view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (list_store));
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree_view),
			-1, "Server", gtk_cell_renderer_text_new (), 
			"text", COLUMN_SERVER, NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree_view),
			-1, "Address", gtk_cell_renderer_text_new (), 
			"text", COLUMN_IP, NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree_view),
			-1, "Port", gtk_cell_renderer_text_new (), 
			"text", COLUMN_PORT, NULL);
	gtk_tree_view_insert_column_with_attributes (GTK_TREE_VIEW (tree_view),
			-1, "Path", gtk_cell_renderer_text_new (), 
			"text", COLUMN_PATH, NULL);
	gtk_container_add (GTK_CONTAINER (window), tree_view);
	gtk_widget_show (tree_view);
	gtk_widget_show (window);

	/* set up query */
	query = gmdns_service_query_new ("http");
	gmdns_service_query_start (query);
	g_signal_connect (query, "answer", (GCallback)answer_callback, NULL);

	/* and go for it */
	gtk_main ();

	return 0;
}
