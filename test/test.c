#include "gmdns.h"
#include "gmdns-service.h"
#include "gmdns-query.h"
#include "gmdns-service-query.h"

#include <arpa/inet.h>

/* testing GmDNSQuery */
void
ptr_answer_cb (GmDNSQuery *query, GmDNSQueryNamedAnswer *answer, void *data) {
	g_message ("(%s) PTR answer: \"%s\" -> \"%s\" ttl=%ld", 
			(const char*)data, answer->name, answer->record_name,
			answer->ttl);
}
void
srv_answer_cb (GmDNSQuery *query, GmDNSQuerySRVAnswer *answer, void *data) {
	g_message ("(%s) SRV answer: \"%s\" -> \"%s\"", 
			(const char*)data, answer->name, answer->record_name);
}
void
test_gmdns_query_PTR (void) {
	GmDNSQuery *ptr_query;
	ptr_query = gmdns_query_new_PTR ("_http._tcp.local.");
	g_signal_connect (ptr_query, "named-answer", (GCallback)ptr_answer_cb, 
			"http");
	gmdns_query_start (ptr_query);
}

void
test_gmdns_query_SRV (void) {
	GmDNSQuery *srv_query;
	srv_query = gmdns_query_new_SRV ("foopy._http._tcp.local.");
	g_signal_connect (srv_query, "srv-answer", (GCallback)srv_answer_cb, 
			"http");
	gmdns_query_start (srv_query);
}

/* testing GmDNSService */
void
test_gmdns_service (void) {
	GmDNSService *service;

	service = gmdns_service_new ("my test", "http", NULL, NULL);
	gmdns_service_set_address (service, inet_addr("127.0.0.1"), 80);
	gmdns_service_start (service);
}

/* testing GmDNSServiceQuery */
static void
answer_callback (GmDNSServiceQuery* 		query, 
		 const GmDNSServiceQueryAnswer*	answer, 
		 gpointer 			data) {
	g_print ("ANSWER!\n");
}

void
test_gmdns_service_query (void) {
	GmDNSServiceQuery *query;

	query = gmdns_service_query_new ("http");
	gmdns_service_query_start (query);
	g_signal_connect (query, "answer", (GCallback)answer_callback, NULL);
}

int
main (int argc, char** argv)
{
	GMainContext *context;
	GMainLoop *loop;

	g_type_init ();

	context = g_main_context_default ();
	gmdns_init (context);

	if (0) test_gmdns_query_PTR ();
	if (0) test_gmdns_query_SRV ();
	if (1) test_gmdns_service ();
	if (0) test_gmdns_service_query ();

	loop = g_main_loop_new (context, FALSE);
	g_main_loop_run (loop);

	return 0;
}
