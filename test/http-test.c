#include "gmdns.h"
#include "gmdns-service.h"

#include <arpa/inet.h>

void
test_gmdns_service (void) {
	GmDNSService *service;

	/* "films" is the name of the service, it can be any UTF-8
	 * character, don't use non-ASCII chars if you need to access it
	 * via command-line tools
	 *
	 * "http" is the name of our protocol
	 */
	service = gmdns_service_new ("films", "http", NULL, NULL);
	/* the IP address is that of the local machine that's exposed to
	 * the network
	 * 
	 * port is self-explanatory */
	gmdns_service_set_address (service, inet_addr("192.168.1.5"), 80);
	/* Some protocols use extra info if available, like http uses "path" */
	gmdns_service_add_extra (service, "path", "/films.php3");

	gmdns_service_start (service);
}

int
main (int argc, char** argv)
{
	GMainContext *context;
	GMainLoop *loop;

	g_type_init ();

	context = g_main_context_default ();
	gmdns_init (context);

	test_gmdns_service ();

	loop = g_main_loop_new (context, FALSE);
	g_main_loop_run (loop);

	return 0;
}
