/* gmdns-query.c
 *
 * Copyright (C) 2003 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gmdns-query.h"
#include "gmdns-private.h"
#include "gmdns-marshal.h"

#include <string.h>

enum
{
	PROP_0,
	/* User-settable */
	PROP_NAME,
	PROP_TYPE,
};

enum
{
	ANSWER,
	A_ANSWER,
	NAMED_ANSWER,
	SRV_ANSWER,
	TXT_ANSWER,
	LAST_SIGNAL
};


struct _GmDNSQueryPrivate
{
	char *name;
	GmDNSType type;
	gboolean querying;
};


static gpointer parent_class = NULL;
static gint gmdns_query_signals[LAST_SIGNAL] = { 0 };


static void gmdns_query_class_init (GmDNSQueryClass * class);
static void gmdns_query_instance_init (GmDNSQuery * query);

static void gmdns_query_finalize (GObject * object);

static void
gmdns_query_get_property (GObject * object,
			  guint property,
			  GValue * value,
			  GParamSpec * param_spec)
{
	GmDNSQuery *conn = GMDNS_QUERY (object);

	switch (property)
	{
	case PROP_NAME:
		g_value_set_string (value, conn->_priv->name);
		break;
	case PROP_TYPE:
		g_value_set_int (value, conn->_priv->type);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
		break;
	}
}

static void
gmdns_query_set_property (GObject * object,
			  guint property,
			  const GValue * value,
			  GParamSpec * param_spec)
{
	GmDNSQuery *conn = GMDNS_QUERY (object);

	switch (property)
	{
	case PROP_NAME:
		if (conn->_priv->name) {
			g_free (conn->_priv->name);
		}
		conn->_priv->name = g_value_dup_string (value);
		break;
	case PROP_TYPE:
		conn->_priv->type = g_value_get_int (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
		break;
	}
}

GType
gmdns_query_get_type (void)
{
	static GType object_type = 0;

	if (!object_type)
	{
		static const GTypeInfo type_info = {
			sizeof (GmDNSQueryClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gmdns_query_class_init,
			(GClassFinalizeFunc) NULL,
			NULL,			/* class data */
			sizeof (GmDNSQuery),
			0,			/* number of pre-allocs */
			(GInstanceInitFunc) gmdns_query_instance_init,
			NULL			/* value table */
		};

		g_type_init ();

		object_type = g_type_register_static (G_TYPE_OBJECT, 
				"GmDNSQuery", &type_info, 0);
	}

	return object_type;
}


static void
gmdns_query_class_init (GmDNSQueryClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->finalize = gmdns_query_finalize;
	object_class->get_property = gmdns_query_get_property;
	object_class->set_property = gmdns_query_set_property;

	gmdns_query_signals[ANSWER] = g_signal_new ("answer", 
			G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_FIRST, 
			G_STRUCT_OFFSET (GmDNSQueryClass, answer), NULL, 
			NULL, gmdns_marshal__VOID__POINTER, G_TYPE_NONE, 1,
			G_TYPE_POINTER);

	gmdns_query_signals[A_ANSWER] = g_signal_new ("a-answer", 
			G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_FIRST, 
			G_STRUCT_OFFSET (GmDNSQueryClass, a_answer), NULL, 
			NULL, gmdns_marshal__VOID__POINTER, G_TYPE_NONE, 1,
			G_TYPE_POINTER);

	gmdns_query_signals[NAMED_ANSWER] = g_signal_new ("named-answer", 
			G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_FIRST, 
			G_STRUCT_OFFSET (GmDNSQueryClass, named_answer), NULL, 
			NULL, gmdns_marshal__VOID__POINTER, G_TYPE_NONE, 1,
			G_TYPE_POINTER);

	gmdns_query_signals[SRV_ANSWER] = g_signal_new ("srv-answer", 
			G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_FIRST, 
			G_STRUCT_OFFSET (GmDNSQueryClass, srv_answer), NULL, 
			NULL, gmdns_marshal__VOID__POINTER, G_TYPE_NONE, 1,
			G_TYPE_POINTER);

	gmdns_query_signals[TXT_ANSWER] = g_signal_new ("txt-answer", 
			G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_FIRST, 
			G_STRUCT_OFFSET (GmDNSQueryClass, txt_answer), NULL, 
			NULL, gmdns_marshal__VOID__POINTER, G_TYPE_NONE, 1,
			G_TYPE_POINTER);

	g_object_class_install_property (object_class, PROP_TYPE, 
			g_param_spec_int ("type", _("Query Type"), 
				_("The record type of the query."), 
				TYPE_MIN, TYPE_MAX, TYPE_INVALID,
				G_PARAM_READABLE));

	g_object_class_install_property (object_class, PROP_NAME, 
			g_param_spec_string ("name", _("Query Name"), 
				_("The dns name being queried."), NULL,
				G_PARAM_READABLE));
}


static void
gmdns_query_instance_init (GmDNSQuery * query)
{
	query->_priv = g_new0 (GmDNSQueryPrivate, 1);
	query->_priv->name = NULL;
	query->_priv->type = TYPE_INVALID;
	query->_priv->querying = FALSE;
}


static void
gmdns_query_finalize (GObject * object)
{
	GmDNSQuery *query = GMDNS_QUERY (object);
	if (query->_priv->querying) {
		gmdns_query_stop (query);
	}
	if (query->_priv->name) {
		g_free (query->_priv->name);
	}
}

/***********************************
 * PRIVATE API                     *
 ***********************************/

/**
 * gmdns_query_notify:
 *
 * This is called by answer_callback in gmdns.c to tell this object to emit
 * signals about a given answer
 */

void 
gmdns_query_notify (GmDNSQuery *query, mdnsda a) {
	GmDNSQueryAnswer *answer;
	GmDNSQueryAAnswer *a_answer;
	GmDNSQueryNamedAnswer *named_answer;
	GmDNSQuerySRVAnswer *srv_answer;
	GmDNSQueryTXTAnswer *txt_answer;

	if (query == NULL) {
		/* mdnsd is having problems. damn you rock-em sock-em robots */
		g_warning ("unexpected query answer for %s,%d", a->name, 
				a->type);
		return;
	}

	/* emit "answer" */
	answer = g_new0 (GmDNSQueryAnswer, 1);
	answer->name = a->name;
	answer->type = a->type;
	answer->ttl = a->ttl;
	answer->data = a->rdata;
	answer->length = a->rdlen;
	g_signal_emit (G_OBJECT (query), gmdns_query_signals[ANSWER], 0, 
			answer);

	switch (answer->type) {
	case TYPE_A:
		a_answer = g_new0 (GmDNSQueryAAnswer, 1);
		a_answer->name = a->name;
		a_answer->type = a->type;
		a_answer->ttl = a->ttl;
		a_answer->ip.s_addr = a->ip;
		g_signal_emit (G_OBJECT (query), gmdns_query_signals[A_ANSWER], 
				0, a_answer);
		g_free (a_answer);
		break;

	case TYPE_NS:
	case TYPE_CNAME:
	case TYPE_PTR:
		named_answer = g_new0 (GmDNSQueryNamedAnswer, 1);
		named_answer->name = a->name;
		named_answer->type = a->type;
		named_answer->ttl = a->ttl;
		named_answer->record_name = a->rdname;
		g_signal_emit (G_OBJECT (query), 
				gmdns_query_signals[NAMED_ANSWER], 0, 
				named_answer);
		g_free (named_answer);
		break;

	case TYPE_SRV:
		srv_answer = g_new0 (GmDNSQuerySRVAnswer, 1);
		srv_answer->name = a->name;
		srv_answer->type = a->type;
		srv_answer->ttl = a->ttl;
		srv_answer->record_name = a->rdname;
		srv_answer->priority = a->srv.priority;
		srv_answer->weight = a->srv.weight;
		srv_answer->port = a->srv.port;
		g_signal_emit (G_OBJECT (query), 
				gmdns_query_signals[SRV_ANSWER], 0, 
				srv_answer);
		g_free (srv_answer);
		break;

	case TYPE_TXT:
		txt_answer = g_new0 (GmDNSQueryTXTAnswer, 1);
		txt_answer->name = a->name;
		txt_answer->type = a->type;
		txt_answer->ttl = a->ttl;

		txt_answer->extra = gmdns_txt_parse (a->rdata, a->rdlen);

		if (txt_answer->extra != NULL) {
			g_signal_emit (G_OBJECT (query), 
					gmdns_query_signals[TXT_ANSWER], 0, 
					txt_answer);
			g_hash_table_destroy (txt_answer->extra);
		}
		g_free (txt_answer);
	default:
		// theres no special case decoder for this type
		break;
	}
}

/***********************************
 * PUBLIC API                      *
 ***********************************/


/**
 * gmdns_query_new:
 *
 * Creates a new #GmDNSQuery-struct.
 * @name: the host of the query
 * @type: the type of the query
 *
 * Returns: a new #GmDNSQuery-struct.
 *
 **/
GmDNSQuery *
gmdns_query_new (const char *name, GmDNSType type)
{
	GmDNSQuery *query = (GmDNSQuery*) g_object_new (GMDNS_TYPE_QUERY, NULL);
	query->_priv->name = g_strdup (name);
	query->_priv->type = type;
	return query;
}

/**
 * gmdns_query_new_A:
 *
 * Creates a new #GmDNSQuery-struct for A type queries
 * @name: the host of the query
 *
 * Returns: a new #GmDNSQuery-struct.
 *
 **/
GmDNSQuery*	
gmdns_query_new_A (const char *host)
{
	g_message ("setting up A query for %s", host);
	return gmdns_query_new (host, TYPE_A);
}

/**
 * gmdns_query_new_NS:
 *
 * Creates a new #GmDNSQuery-struct for NS type queries
 * @name: the host of the query
 *
 * Returns: a new #GmDNSQuery-struct.
 *
 **/

GmDNSQuery* 
gmdns_query_new_NS (const char *host)
{
	g_message ("setting up NS query for %s", host);
	return gmdns_query_new (host, TYPE_NS);
}

/**
 * gmdns_query_new_CNAME:
 *
 * Creates a new #GmDNSQuery-struct for CNAME type queries
 * @name: the host of the query
 *
 * Returns: a new #GmDNSQuery-struct.
 *
 **/
GmDNSQuery* 
gmdns_query_new_CNAME (const char *host)
{
	g_message ("setting up CNAME query for %s", host);
	return gmdns_query_new (host, TYPE_CNAME);
}

/**
 * gmdns_query_new_PTR:
 *
 * Creates a new #GmDNSQuery-struct for PTR type queries
 * @name: the host of the query
 *
 * Returns: a new #GmDNSQuery-struct.
 *
 **/
GmDNSQuery* 
gmdns_query_new_PTR (const char *host)
{
	g_message ("setting up PTR query for %s", host);
	return gmdns_query_new (host, TYPE_PTR);
}

/**
 * gmdns_query_new_SRV:
 *
 * Creates a new #GmDNSQuery-struct for SRV type queries
 * @name: the host of the query
 *
 * Returns: a new #GmDNSQuery-struct.
 *
 **/
GmDNSQuery* 
gmdns_query_new_SRV (const char *host)
{
	g_message ("setting up SRV query for %s", host);
	return gmdns_query_new (host, TYPE_SRV);
}

/**
 * gmdns_query_new_TXT:
 *
 * Creates a new #GmDNSQuery-struct for TXT type queries
 * @name: the host of the query
 *
 * Returns: a new #GmDNSQuery-struct.
 *
 **/
GmDNSQuery* 
gmdns_query_new_TXT (const char *host)
{
	g_message ("setting up TXT query for %s", host);
	return gmdns_query_new (host, TYPE_TXT);
}

/**
 * gmdns_query_start:
 * @query: the #GmDNSQuery
 *
 * Start the query
 *
 **/
void
gmdns_query_start (GmDNSQuery * query)
{
	g_return_if_fail (query != NULL);
	g_return_if_fail (GMDNS_IS_QUERY (query));
	g_return_if_fail (query->_priv->querying == FALSE);

	gmdns_aquery_add (query->_priv->type, query->_priv->name, query);
	query->_priv->querying = TRUE;
}

/**
 * gmdns_query_stop:
 * @query: the #GmDNSQuery
 *
 * Stop the query
 *
 **/
void
gmdns_query_stop (GmDNSQuery * query)
{
	g_return_if_fail (query != NULL);
	g_return_if_fail (GMDNS_IS_QUERY (query));
	g_return_if_fail (query->_priv->querying == TRUE);

	gmdns_aquery_remove (query->_priv->type, query->_priv->name, query);
        query->_priv->querying = FALSE;
}
