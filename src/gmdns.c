/* gmdns.c
 *
 * Copyright (C) 2003 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <fcntl.h>
#include <errno.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>

#include "mdnsd.h"

#include "gmdns.h"
#include "gmdns-private.h"

#undef DEBUG_PACKETS
#undef DEBUG_CALLBACKS

struct GmDNSSharedData gmdns_shared_data = {
	0, 	/* initialized */
	NULL, 	/* d */
	NULL, 	/* mainloop */
	-1,	/* sock */
	NULL, 	/* sock_channel */
	NULL, 	/* sock_source */
	NULL, 	/* timeout_source */
	NULL	/* active_queries */
};

/* a structure that represents an active query */
typedef struct _ActiveQuery {
	GmDNSType type;
	char *name;
	GList *listeners; /* GmDNSQuery objects who are interested */
} ActiveQuery;

#ifdef DEBUG_PACKETS
#include <ctype.h>
static void 
hexdump (const char* prefix, const char* data, size_t len)
{
	int ROWLEN=(78-strlen(prefix))/4;
	int row, col;

	for (row=0; row <= len/ROWLEN; row++) {
		printf ("%s ", prefix);
		for (col=0; col<ROWLEN && (col+row*ROWLEN)<len; col++) {
			printf ("%02X ", data[col+row*ROWLEN]);
		}
		for (; col<ROWLEN; col++) {
			printf ("   ");
		}
		for (col=0; col<ROWLEN && (col+row*ROWLEN)<len; col++) {
			int c = data[col+row*ROWLEN];
			printf ("%c", isprint(c)?c:'?');
		}
		printf ("\n");
	}
}
#else
static void 
hexdump (const char* prefix, const char* data, size_t len)
{
}
#endif

/* Utility function for the name/value pair from the TXT record */
gboolean
gmdns_str_ascii_case_equals (gconstpointer v, gconstpointer v2)
{
	return (g_ascii_strcasecmp (v, v2) == 0);
}

/* create multicast 224.0.0.251:5353 socket */
static int 
msock (void)
{
	int s, flag = 1, ittl = 255;
	struct sockaddr_in in;
	struct ip_mreq mc;
	char ttl = 255;

	memset(&in, 0, sizeof(in));
	in.sin_family = AF_INET;
	in.sin_port = htons(5353);
	in.sin_addr.s_addr = 0;

	if((s = socket(AF_INET,SOCK_DGRAM,0)) < 0) return 0;
#ifdef SO_REUSEPORT
	setsockopt(s, SOL_SOCKET, SO_REUSEPORT, (char*)&flag, sizeof(flag));
#endif
	setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char*)&flag, sizeof(flag));
	if(bind(s,(struct sockaddr*)&in,sizeof(in))) { close(s); return 0; }

	mc.imr_multiaddr.s_addr = inet_addr("224.0.0.251");
	mc.imr_interface.s_addr = htonl(INADDR_ANY);
	setsockopt(s, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mc, sizeof(mc)); 
	setsockopt(s, IPPROTO_IP, IP_MULTICAST_TTL, &ttl, sizeof(ttl));
	setsockopt(s, IPPROTO_IP, IP_MULTICAST_TTL, &ittl, sizeof(ittl));

	flag =  fcntl(s, F_GETFL, 0);
	flag |= O_NONBLOCK;
	fcntl(s, F_SETFL, flag);

	return s;
}

/**
 * This callback is called as a io callback when a packet is has been recieved
 * and is ready to be processed.
 */
static gboolean
input_callback (GIOChannel *source, GIOCondition condition, gpointer data)
{
	struct sockaddr_in from;
	struct message m;
	unsigned char buf[MAX_PACKET_LEN];
	int bsize, ssize = sizeof(struct sockaddr_in);
	gboolean recieved_messages = FALSE;

#ifdef DEBUG_CALLBACKS
	g_message ("input_callback");
#endif

	gmdns_shared_data.timeout_source = NULL;

	while ((bsize = recvfrom (gmdns_shared_data.sock, buf, MAX_PACKET_LEN, 0,  
					(struct sockaddr*)&from, &ssize)) > 0) {
#ifdef DEBUG_CALLBACKS
		g_message (" read %d bytes", bsize);
#endif
		hexdump ("recv", buf, bsize);
		memset (&m, 0, sizeof (struct message));
		message_parse (&m, buf);
		mdnsd_in (gmdns_shared_data.d, &m,  
				(unsigned long int)from.sin_addr.s_addr, 
				from.sin_port);
		recieved_messages = TRUE;
	}
	if (recieved_messages) {
		// perhaps one of these received messages means we have to
		// send something (eg: a response to a request received
		gmdns_events_pending ();
	}
	if (bsize < 0 && errno != EAGAIN) { 
		g_error ("can't read from socket: %s", strerror (errno));
		/* FIXME: error callback */
		return FALSE; 
	}
	return TRUE;
}

/**
 * This callback is called as a glib mainloop timeout when a packet is to be
 * sent to the network.
 **/
static gboolean
output_callback (gpointer data)
{
	struct sockaddr_in to;
	unsigned long ip;
	unsigned short port;
	struct message m;
	int len;

#ifdef DEBUG_CALLBACKS
	g_message ("output_callback (timeout_source = %p)", gmdns_shared_data.timeout_source);
#endif

	/* we're done with this source */
	gmdns_shared_data.timeout_source = NULL;

	while (mdnsd_out (gmdns_shared_data.d, &m, &ip, &port))
	{
		memset (&to, 0, sizeof (to));
		to.sin_family = AF_INET;
		to.sin_port = port;
		to.sin_addr.s_addr = ip;
		len = sendto (gmdns_shared_data.sock, message_packet (&m), 
				message_packet_len (&m), 0,  
				(struct sockaddr *)&to, sizeof (to));
		if (len != message_packet_len (&m)) {
			g_error ("can't write to socket: %s", strerror (errno));
			/* FIXME: error callback */
			return FALSE;
		}
#ifdef DEBUG_CALLBACKS
		g_message ("  packet sent (len = %d)", len);
#endif
		hexdump ("send", message_packet (&m), message_packet_len (&m));
	}
	// we might need to send another callback
	gmdns_events_pending ();
	return FALSE;
}

/**
 * This aborts the pending output (timeout) callback if there is one and
 * resets its entry in gmdns_shared_data.
 */
static void
remove_output_callback (void) {
	if (gmdns_shared_data.timeout_source == NULL) {
		return;
	}
#ifdef DEBUG_CALLBACKS
	g_message ("cancelling the output callback");
#endif
	g_source_destroy (gmdns_shared_data.timeout_source);
	g_source_unref (gmdns_shared_data.timeout_source);
	gmdns_shared_data.timeout_source = NULL;
}

/**
 * This aborts the pending output (timeout) callback if there is one and
 * resets its entry in gmdns_shared_data.
 */
static void
remove_input_callback (void) {
	if (gmdns_shared_data.sock_source == NULL) {
		return;
	}
	g_source_destroy (gmdns_shared_data.sock_source);
	g_source_unref (gmdns_shared_data.sock_source);
	gmdns_shared_data.sock_source = NULL;
}

/**
 * This aborts the pending callback (if one exists) and schedules a new
 * timeout callback for when the mdnsd engine thinks that it'll want to send
 * a new packet (for stuff like renewing/expiring entries and the like).
 */
static void
setup_output_callback (void)
{
	/* this ends up being a pointer into the mdsnd structure */
	struct timeval *tv; 

	/* GLib wants to know the time in milliseconds */
	int milliseconds;

	/* remove pending callbacks */
	remove_output_callback ();

	tv = mdnsd_sleep(gmdns_shared_data.d);
	milliseconds = tv->tv_sec*1000 + tv->tv_usec/1000;

#ifdef DEBUG_CALLBACKS
	g_message ("setting up output callback (%d msec)", milliseconds);
#endif

	if (milliseconds <= 0) {
		gmdns_shared_data.timeout_source =
			g_idle_source_new ();
	} else {
		gmdns_shared_data.timeout_source = 
			g_timeout_source_new (milliseconds);
	}

	g_source_set_callback (gmdns_shared_data.timeout_source,
			output_callback, NULL, NULL);
	g_source_attach (gmdns_shared_data.timeout_source,
			gmdns_shared_data.mainloop);
}

/**
 * This sets up a read callback for the current socket.
 */
static void
setup_input_callback (void)
{
	/* remove pending callbacks */
	remove_input_callback ();

	gmdns_shared_data.sock_source = 
		g_io_create_watch (gmdns_shared_data.sock_channel, G_IO_IN);
	g_source_set_callback (gmdns_shared_data.sock_source, 
			(GSourceFunc) input_callback, NULL, NULL);
	g_source_attach (gmdns_shared_data.sock_source,
			gmdns_shared_data.mainloop);
}
gboolean 
gmdns_init (GMainContext *loop)
{
	if (gmdns_shared_data.initialized) {
		/* already initialized */
		return TRUE;
	}

	gmdns_shared_data.d = mdnsd_new (1,1000);
	gmdns_shared_data.sock = msock ();
	if (gmdns_shared_data.sock < 0) {
		g_error ("can't create socket: %s", strerror (errno));
		return FALSE;
	}
	gmdns_shared_data.sock_channel = 
		g_io_channel_unix_new (gmdns_shared_data.sock);

	gmdns_shared_data.mainloop = loop;
	gmdns_shared_data.sock_source = NULL;
	gmdns_shared_data.timeout_source = NULL;

	setup_input_callback ();
	setup_output_callback ();

	gmdns_shared_data.initialized = TRUE;
	return TRUE;
}

void
gmdns_shutdown (void)
{
	/* FIXME: discard state? */

	/* remove pending callbacks */
	setup_input_callback ();
	setup_output_callback ();

	/* FIXME: close socket */

	mdnsd_shutdown (gmdns_shared_data.d);
	mdnsd_free (gmdns_shared_data.d);
}

void
gmdns_events_pending (void)
{
	setup_output_callback ();
}

/**
 * Loop up a query and return it. 
 * The global state lock should be held when you call this.
 */
static ActiveQuery *
gmdns_aquery_lookup (GmDNSType type, const char* name) {
	GList *iter = gmdns_shared_data.active_queries;
	ActiveQuery *query;
	while (iter) {
		query = (ActiveQuery*)iter->data;
		if (query->type == type && !strcmp (query->name, name)) {
			/* we have a weiner */
			return query;
		}
		iter = g_list_next (iter);
	}
	return NULL;
}

static int
answer_callback (mdnsda a, void* arg) {
	ActiveQuery *aq = (ActiveQuery*) arg;
	GList *iter;

	g_message ("got an answer");

	GMDNS_ENTER
	iter = aq->listeners;
	while (iter) {
		if (iter->data)
			gmdns_query_notify ((GmDNSQuery*)iter->data, a);
		iter = g_list_next (iter);
	}
	GMDNS_LEAVE
	return 0;
}

void 
gmdns_aquery_add (GmDNSType type, const char* name, GmDNSQuery *query) {
	ActiveQuery *aq;

	GMDNS_ENTER
	aq = gmdns_aquery_lookup (type, name);
	if (!aq) {
		/* There exists no query for this (type,name) */

		/* so lets allocate the structure */
		aq = g_new0 (ActiveQuery, 1);
		aq->type = type;
		aq->name = g_strdup (name);
		aq->listeners = g_list_prepend (NULL, query);

		/* add it to the list of active queries */
		gmdns_shared_data.active_queries =
			g_list_prepend (gmdns_shared_data.active_queries, aq);

		g_message ("mdnsd_query (%s, %d, %p, %p)\n", name, type, answer_callback, aq);
		/* and we set up the query in mdnsd */
		/* NOTE: mdnsd's API isn't const correct */
		mdnsd_query (GMDNS_MDNSD, (char*) name, type, answer_callback,
				aq);
		gmdns_events_pending ();
		
	} else {
		/* add the supplied query as a listener */
		aq->listeners = g_list_prepend (aq->listeners, query);

		/* FIXME: do we need to extract entries from the cache and send
		 * callbacks. */
	}

	GMDNS_LEAVE
}

void 
gmdns_aquery_remove (GmDNSType type, const char* name, GmDNSQuery *query) {
	ActiveQuery *aq;

	GMDNS_ENTER
	aq = gmdns_aquery_lookup (type, name);
	if (!aq) {
		g_warning ("failed to loop up query for (%d,\"%s\")", 
				type, name);
		GMDNS_LEAVE
		return;
	}

	/* remove this query from the list of listeners */
	aq->listeners = g_list_remove (aq->listeners, query);

	/* if there are no more listeners */
	if (aq->listeners == NULL) {
		g_message ("mdnsd_query (%s, %d, %p, %p)\n", name, type, (void*)NULL, (void*)NULL);
		/* stop the query */
		mdnsd_query (GMDNS_MDNSD, (char*) name, type, NULL, NULL);

		/* remove aq from the list of queries */
		gmdns_shared_data.active_queries =
			g_list_remove (gmdns_shared_data.active_queries, aq);

		/* and free the data structure */
		g_free (aq->name);
		g_free (aq);
	}
	GMDNS_LEAVE
}
