/* gmdns-record.c
 *
 * Copyright (C) 2003 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gmdns-record.h"
#include "gmdns-private.h"
#include "gmdns-marshal.h"

enum
{
	PROP_0,
	/* User-settable */
	PROP_TYPE,
	PROP_NAME,
	PROP_HOST,
	PROP_PORT,

	/* Considered read-only */
	PROP_PUBLISHED
};

enum
{
	CONFLICT,
	LAST_SIGNAL
};


struct _GmDNSRecordPrivate
{
	mdnsdr record;
};


static gpointer parent_class = NULL;
static gint gmdns_record_signals[LAST_SIGNAL] = { 0 };


static void gmdns_record_class_init (GmDNSRecordClass * class);
static void gmdns_record_instance_init (GmDNSRecord * record);

static void gmdns_record_finalize (GObject * object);

GType
gmdns_record_get_type (void)
{
	static GType object_type = 0;

	if (!object_type)
	{
		static const GTypeInfo type_info = {
			sizeof (GmDNSRecordClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gmdns_record_class_init,
			(GClassFinalizeFunc) NULL,
			NULL,			/* class data */
			sizeof (GmDNSRecord),
			0,			/* number of pre-allocs */
			(GInstanceInitFunc) gmdns_record_instance_init,
			NULL			/* value table */
		};

		g_type_init ();

		object_type = g_type_register_static (G_TYPE_OBJECT, 
				"GmDNSRecord", &type_info, 0);
	}

	return object_type;
}


static void
gmdns_record_class_init (GmDNSRecordClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->finalize = gmdns_record_finalize;

	gmdns_record_signals[CONFLICT] = g_signal_new ("conflict", 
			G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_FIRST, 
			G_STRUCT_OFFSET (GmDNSRecordClass, conflict), NULL, 
			NULL, gmdns_marshal__VOID__STRING_INT, G_TYPE_NONE, 0);
}


static void
gmdns_record_instance_init (GmDNSRecord * record)
{
	record->_priv = g_new0 (GmDNSRecordPrivate, 1);
	record->_priv->record = NULL;
}


static void
gmdns_record_finalize (GObject * object)
{
	GmDNSRecord *record = GMDNS_RECORD (object);
	if (record->_priv->record) {
		GMDNS_ENTER
		mdnsd_done (GMDNS_MDNSD, record->_priv->record);
		GMDNS_LEAVE
		gmdns_events_pending ();
	}
}

/**
 * conflict_callback:
 *
 * This callback is called by the mdnsd code when a conflict with a requested
 * unique record is found. We emit a "conflict" signal to handle this.
 */
static void
conflict_callback (char* host, int type, void* arg)
{ 
	GmDNSRecord *record = GMDNS_RECORD (arg);

	g_message ("CONFLICT (%s, %d)", host, type);

	g_signal_emit (G_OBJECT (record), gmdns_record_signals[CONFLICT], 0,
			host, type);
}


/***********************************
 * PUBLIC API                      *
 ***********************************/


/**
 * gmdns_record_new:
 *
 * Creates a new, unopened #GmDNSRecord-struct.
 * @host: the host of the record
 * @type: the type of the record
 * @ttl: the time to live of the record
 * @unique: should the record be unique
 *
 * Returns: a new #GmDNSRecord-struct.
 *
 **/
GmDNSRecord *
gmdns_record_new (const char *host, int type, long int ttl, gboolean unique)
{
	GmDNSRecord *r = (GmDNSRecord*) g_object_new (GMDNS_TYPE_RECORD, NULL);

	GMDNS_ENTER
	if (unique) {
		/* mdnsd fails to declare host as const :-( */
		r->_priv->record = mdnsd_unique (GMDNS_MDNSD, (char*) host, 
				type, ttl, conflict_callback, r);
	} else {
		/* mdnsd fails to declare host as const :-( */
		r->_priv->record = mdnsd_shared (GMDNS_MDNSD, (char*)host, 
				type, ttl);
	}
	GMDNS_LEAVE
	gmdns_events_pending ();

	return r;
}


/**
 * gmdns_record_set_raw:
 * @record: the #GmDNSRecord to set data for
 * @data: the raw data to return for query for @record
 * @len: the length of that raw data
 *
 * Sets the data that a #GmDNSRecord points to.
 *
 **/
void
gmdns_record_set_data (GmDNSRecord * record, const char* data, int len)
{
	g_return_if_fail (record != NULL);
	g_return_if_fail (GMDNS_IS_RECORD (record));
	g_return_if_fail (data != NULL);
	g_return_if_fail (len > 0);

	GMDNS_ENTER
	/* NOTE: mdnsd's api isn't const correct */
	mdnsd_set_raw (GMDNS_MDNSD, record->_priv->record, (char*)data, len);
	GMDNS_LEAVE
	gmdns_events_pending ();
}


/**
 * gmdns_record_set_host:
 * @record: the #GmDNSRecord to set host for
 * @host: the host to return for query for @record
 *
 * Sets the host that a #GmDNSRecord points to.
 *
 **/
void
gmdns_record_set_host (GmDNSRecord * record, const char* host)
{
	g_return_if_fail (record != NULL);
	g_return_if_fail (GMDNS_IS_RECORD (record));
	g_return_if_fail (host != NULL);

	GMDNS_ENTER
	/* NOTE: mdnsd's api isn't const correct */
	mdnsd_set_host (GMDNS_MDNSD, record->_priv->record, (char*)host);
	GMDNS_LEAVE
	gmdns_events_pending ();
}


/**
 * gmdns_record_set_ip:
 * @record: the #GmDNSRecord to set ip for
 * @ip: the ip to return for query for @record
 *
 * Sets the ip that a #GmDNSRecord points to.
 *
 **/
void
gmdns_record_set_ip (GmDNSRecord * record, unsigned long int ip)
{
	g_return_if_fail (record != NULL);
	g_return_if_fail (GMDNS_IS_RECORD (record));

	GMDNS_ENTER
	//mdnsd_set_ip (GMDNS_MDNSD, record->_priv->record, ip);
	mdnsd_set_raw (GMDNS_MDNSD, record->_priv->record, 
			(unsigned char*)&ip, 4);
	GMDNS_LEAVE
	gmdns_events_pending ();
}


/**
 * gmdns_record_set_srv:
 * @record: the #GmDNSRecord to set data for
 * @priority: the priority to return for query for @record
 * @weight: the weight to return for query for @record
 * @port: the port to return for query for @record
 * @name: the name to return for query for @record
 *
 * Sets the service record that a #GmDNSRecord points to.
 *
 **/
void
gmdns_record_set_srv (GmDNSRecord* record, int priority, int weight, 
		int port, const char* name)
{
	g_return_if_fail (record != NULL);
	g_return_if_fail (GMDNS_IS_RECORD (record));
	g_return_if_fail (name != NULL);
	g_return_if_fail (port >= 0 && port <= 0xFFFF);

	/* FIXME: range check priority, weight */

	GMDNS_ENTER
	/* mdnsd fails to declare host as const :-( */
	mdnsd_set_srv (GMDNS_MDNSD, record->_priv->record, priority, weight,
			port, (char*) name);
	GMDNS_LEAVE
	gmdns_events_pending ();
}

/**
 * gmdns_record_set_txt
 * @record: the #GmDNSRecord to set data for
 * @hash: the hash table of values
 *
 * Builds a DNS Service Discovery style TXT message for the supplied name /
 * value pairs.
 */
void
gmdns_record_set_txt (GmDNSRecord* record, const GHashTable *hash)
{
	unsigned char *txt;
	size_t len;

	g_return_if_fail (record != NULL);
	g_return_if_fail (GMDNS_IS_RECORD (record));
	g_return_if_fail (hash != NULL);

	gmdns_txt_build (hash, &txt, &len);
	
	GMDNS_ENTER
	/* NOTE: mdnsd's api isn't const correct */
	mdnsd_set_raw (GMDNS_MDNSD, record->_priv->record, txt, len);
	GMDNS_LEAVE
	
	g_free (txt);

	gmdns_events_pending ();
}
