#ifndef __GDNS_IBUFFER_H__
#define __GDNS_IBUFFER_H__

#include <glib.h>

/*
 * A GDNSIBuffer is a hany data structure that knows how to parse rfc1035 data
 * out of itself.
 */
struct _GDNSIBuffer {
	unsigned char* data;
	int len;
	int off;
};
typedef struct _GDNSIBuffer GDNSIBuffer;

/*
 * Create a new GDNSIBuffer containing the supplied data.
 */
GDNSIBuffer* gdns_ibuffer_new (const unsigned char* data, int len);

/*
 * Discard a buffer and discard the memory associated with it.
 */
void gdns_ibuffer_free (GDNSIBuffer* buf);

/*
 * Seek the buffer to a particular offset.
 */
void gdns_ibuffer_seek (GDNSIBuffer* buf, int off);

/*
 * Return the current position in the buffer
 */
int gdns_ibuffer_tell (GDNSIBuffer* buf);

/*
 * Parses out a 16 bit unsigned int in network byteorder and advances the
 * offset pointer 2 bytes.
 */
guint16 gdns_ibuffer_get_short (GDNSIBuffer* buf);

/*
 * Parses out a 32 bit unsigned int in network byteorder and advances the
 * offset pointer 4 bytes.
 */
guint32 gdns_ibuffer_get_long (GDNSIBuffer* buf);

/*
 * Returns the next byte to be parsed.
 */
guint8 gdns_ibuffer_peek_byte (GDNSIBuffer* buf);

/*
 * Returns the next byte and advances the offset by one byte.
 */
guint8 gdns_ibuffer_get_byte (GDNSIBuffer* buf);

/*
 * Return a pointer to the current position int he buffer and advance the
 * offset the specified amount.
 */
const unsigned char* gdns_ibuffer_get_bytes_no_copy (GDNSIBuffer* buf, int length);

/*
 * Return a @length bytes from the buffer and advance the
 * offset that amount.
 */
unsigned char* gdns_ibuffer_get_bytes (GDNSIBuffer* buf, int length);

/*
 * Parses a domain name out of the buffer.
 */
char* gdns_ibuffer_get_domain_name (GDNSIBuffer* buf);


#endif
