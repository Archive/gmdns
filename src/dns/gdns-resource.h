/* vi: sw=4 ts=4
 */
#ifndef __GDNS_RESOURCE_H__
#define __GDNS_RESOURCE_H__

#include <glib-object.h>
#include <time.h>

#include "gdns.h"
#include "gdns-ibuffer.h"
#include "gdns-obuffer.h"

#define GDNS_TYPE_RESOURCE                        (gdns_resource_get_type ())
#define GDNS_RESOURCE(obj)                        (G_TYPE_CHECK_INSTANCE_CAST ((obj), GDNS_TYPE_RESOURCE, GDNSResource))
#define GDNS_RESOURCE_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), GDNS_TYPE_RESOURCE, GDNSResourceClass))
#define GDNS_IS_RESOURCE(obj)                     (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDNS_TYPE_RESOURCE))
#define GDNS_IS_RESOURCE_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), GDNS_TYPE_RESOURCE))
#define GDNS_RESOURCE_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), GDNS_TYPE_RESOURCE, GDNSResourceClass))
                                                                                
                                                                                
G_BEGIN_DECLS
                                                                                
typedef struct _GDNSResource GDNSResource;
typedef struct _GDNSResourceClass GDNSResourceClass;
typedef struct _GDNSResourcePrivate GDNSResourcePrivate;
                                                                                
struct _GDNSResource
{
	GObject parent;
                                                                                
	unsigned char *name;
	GDNSType type;
	GDNSClass klass;
	unsigned long int ttl;
	unsigned short rdlength;
	unsigned char* rdata;

	enum {
		RDATA_RAW, /* rdata has not been parsed */
		RDATA_A,	/* an A record has been parsed */
		RDATA_NAME,	/* an name format record has been parsed */
		RDATA_SRV,	/* a SRV record has been parsed */
	} rdata_format;
		
    union {
        struct { guint32 ip; } a;
        struct { unsigned char *name; } name;
        struct { unsigned short int priority, weight, port; unsigned char *name; } srv;
    } rdata_parsed;



	time_t expiry; /* ttl + time() when this was recieved */
};
                                                                                
struct _GDNSResourceClass
{
	GObjectClass parent_class;
};
                                                                                
GType gdns_resource_get_type (void);

GDNSResource* gdns_resource_new (const char* name, GDNSType type, GDNSClass klass, unsigned long int ttl);
GDNSResource* gdns_resource_new_A (const char* name, guint32 ip, unsigned long int ttl);
GDNSResource* gdns_resource_new_NS (const char* name, const char* target, unsigned long int ttl);
GDNSResource* gdns_resource_parse (GDNSIBuffer* buffer);
void gdns_resource_unparse (GDNSResource* resource, GDNSOBuffer* buf);

G_END_DECLS

#endif
