/* vim: ts=4 sw=4
 */
#include <string.h> 

#include "gdns-obuffer.h"

/** Ensure that @buf has at least enough space free in it to store @length
 * bytes */
static void
_gdns_obuffer_ensure_space (GDNSOBuffer* buf, int length)
{
	guint8* new_bytes;

	if (length > buf->bytes_length) {
		/* not enough space */
		new_bytes = g_malloc0 (buf->bytes_length*2);
		memcpy (new_bytes, buf->bytes, buf->data_length);

		buf->bytes_length = buf->bytes_length*2;

		g_free (buf->bytes);
		buf->bytes = new_bytes;
	}
}

/** Add @length bytes from @bytes to @buf advancing the offset */
static void
_gdns_obuffer_put (GDNSOBuffer* buf, const guint8* bytes, int length)
{
	_gdns_obuffer_ensure_space (buf, buf->off+length);
	memcpy (buf->bytes+buf->off, bytes, length);
	buf->off += length;
	if (buf->off > buf->data_length) {
		buf->data_length = buf->off;
	}
}

#define INITIAL_BUFFER_SIZE 256

GDNSOBuffer* 
gdns_obuffer_new ()
{
	GDNSOBuffer* buf;
	buf = g_new0 (GDNSOBuffer, 1);
	buf->bytes = g_malloc0 (INITIAL_BUFFER_SIZE);
	buf->bytes_length = INITIAL_BUFFER_SIZE;
	buf->data_length = 0;
	buf->off = 0;
	return buf;
}

void 
gdns_obuffer_free (GDNSOBuffer* buf)
{
	g_assert (buf != NULL);

	g_free (buf->bytes);
	g_free (buf);
}


void 
gdns_obuffer_get_buffer (GDNSOBuffer* buf, guint8** buffer, int* length)
{
	g_assert (buf != NULL);
	g_assert (buffer != NULL);

	*buffer = g_malloc0 (buf->data_length);
	memcpy (*buffer, buf->bytes, buf->data_length);

	*length = buf->data_length;
}


int 
gdns_obuffer_tell (GDNSOBuffer* buf)
{
	g_assert (buf != NULL);

	return buf->off;
}


void
gdns_obuffer_seek (GDNSOBuffer* buf, int off)
{
	g_assert (buf != NULL);

	_gdns_obuffer_ensure_space (buf, off);
	if (buf->data_length < off) {
		buf->data_length = off;
	}
	buf->off = off;
}


void 
gdns_obuffer_put_byte (GDNSOBuffer* buf, guint8 x)
{
	g_assert (buf != NULL);

	_gdns_obuffer_put (buf, &x, 1);
}


void 
gdns_obuffer_put_short (GDNSOBuffer* buf, guint16 x)
{
	guint16 network;
	
	g_assert (buf != NULL);

	network = g_htons (x);
	_gdns_obuffer_put (buf, (guint8*)&network, 2);
}


void 
gdns_obuffer_put_long (GDNSOBuffer* buf, guint32 x)
{
	guint32 network;

	g_assert (buf != NULL);

	network = g_htonl (x);
	_gdns_obuffer_put (buf, (guint8*)&network, 4);
}


void 
gdns_obuffer_put_bytes (GDNSOBuffer* buf, const guint8* bytes, int length)
{
	g_assert (buf != NULL);
	g_assert (bytes != NULL);
	g_assert (length > 0);

	_gdns_obuffer_put (buf, bytes, length);
}


void 
gdns_obuffer_put_domain_name (GDNSOBuffer* buf, const char* name)
{
	/* FIXME: use DNS label compression */

	char** pieces;
	int i, l;

	g_assert (buf != NULL);
	g_assert (name != NULL);
	g_assert (strlen(name) < 254);

	pieces = g_strsplit (name, ".", -1);

	for (i=0; pieces[i]; i++) {
		l = strlen (pieces[i]);
		if (l > 127) {
			g_warning ("Component \"%s\" of \"%s\" is longer than 127 bytes. This is forbidden by DNS RFC 1035.", pieces[i], name);
			continue;
		}
		gdns_obuffer_put_byte (buf, (guint8)l);
		_gdns_obuffer_put (buf, pieces[i], l);
	}
	gdns_obuffer_put_byte (buf, (guint8)0); /* terminator */

	g_strfreev (pieces);
}
