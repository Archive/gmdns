#ifndef __GDNS_QUESTION_H__
#define __GDNS_QUESTION_H__

#include <glib-object.h>

#include "gdns.h"
#include "gdns-ibuffer.h"
#include "gdns-obuffer.h"

#define GDNS_TYPE_QUESTION                        (gdns_question_get_type ())
#define GDNS_QUESTION(obj)                        (G_TYPE_CHECK_INSTANCE_CAST ((obj), GDNS_TYPE_QUESTION, GDNSQuestion))
#define GDNS_QUESTION_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), GDNS_TYPE_QUESTION, GDNSQuestionClass))
#define GDNS_IS_QUESTION(obj)                     (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDNS_TYPE_QUESTION))
#define GDNS_IS_QUESTION_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), GDNS_TYPE_QUESTION))
#define GDNS_QUESTION_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), GDNS_TYPE_QUESTION, GDNSQuestionClass))
                                                                                
                                                                                
G_BEGIN_DECLS
                                                                                
typedef struct _GDNSQuestion GDNSQuestion;
typedef struct _GDNSQuestionClass GDNSQuestionClass;
typedef struct _GDNSQuestionPrivate GDNSQuestionPrivate;
                                                                                
struct _GDNSQuestion
{
        GObject parent;
                                                                                
	unsigned char *name;
	GDNSType type:16;
	GDNSClass klass:16;
};
                                                                                
struct _GDNSQuestionClass
{
        GObjectClass parent_class;
};
                                                                                
GType gdns_question_get_type (void);

GDNSQuestion* gdns_question_new (const char* name, GDNSType type, GDNSClass klass);
GDNSQuestion* gdns_question_parse (GDNSIBuffer* buf);
void gdns_question_unparse (GDNSQuestion* question, GDNSOBuffer* buf);

G_END_DECLS

#endif
