/* vi: sw=4 ts=4
 */
#include "gdns-resource.h"


/*** GOBJECT MAGIC ***/
static void
gdns_resource_finalize (GObject * object)
{
	GDNSResource *resource = GDNS_RESOURCE (object);
	if (resource->name) {
		g_free (resource->name);
	}
}


static void
gdns_resource_class_init (GDNSResourceClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	/*parent_class = g_type_class_peek_parent (class);*/

	object_class->finalize = gdns_resource_finalize;
}


GType
gdns_resource_get_type (void)
{
	static GType object_type = 0;

	if (!object_type)
	{
		static const GTypeInfo type_info = {
			sizeof (GDNSResourceClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gdns_resource_class_init,
			(GClassFinalizeFunc) NULL,
			NULL,			/* class data */
			sizeof (GDNSResource),
			0,			/* number of pre-allocs */
			(GInstanceInitFunc) NULL,
			NULL			/* value table */
		};

		g_type_init ();

		object_type = g_type_register_static (G_TYPE_OBJECT, "GDNSResource", 
				&type_info, 0);
	}

	return object_type;
}


/*** PUBLIC API ***/
GDNSResource* 
gdns_resource_new (const char* name, 
		    GDNSType type,
			GDNSClass klass,
			unsigned long int ttl)
{
	GDNSResource* resource;

	resource = g_object_new (gdns_resource_get_type (), NULL);
	resource->name = g_strdup (name);
	resource->type = type;
	resource->klass = klass;
	resource->ttl = ttl;
	resource->expiry = resource->ttl + time (NULL);

	return resource;
}


GDNSResource*
gdns_resource_new_A (const char* name, guint32 ip, unsigned long int ttl)
{
	GDNSResource* resource = gdns_resource_new (name, TYPE_A, CLASS_IN, ttl);
	resource->rdata_format = RDATA_A;
	resource->rdata_parsed.a.ip = ip;
	resource->rdata = NULL;
	resource->rdlength = 0;
	return resource;
}


GDNSResource*
gdns_resource_new_NS (const char* name, const char* target, unsigned long int ttl)
{
	GDNSResource* resource = gdns_resource_new (name, TYPE_NS, CLASS_IN, ttl);
	resource->rdata_format = RDATA_NAME;
	resource->rdata_parsed.name.name = g_strdup (target);
	resource->rdata = NULL;
	resource->rdlength = 0;
	return resource;
}


GDNSResource*
gdns_resource_new_CNAME (const char* name, const char* target, unsigned long int ttl)
{
	GDNSResource* resource = 
		gdns_resource_new (name, TYPE_CNAME, CLASS_IN, ttl);
	resource->rdata_format = RDATA_NAME;
	resource->rdata_parsed.name.name = g_strdup (target);
	resource->rdata = NULL;
	resource->rdlength = 0;
	return resource;
}


GDNSResource*
gdns_resource_new_PTR (const char* name, const char* target, unsigned long int ttl)
{
	GDNSResource* resource = gdns_resource_new (name, TYPE_PTR, CLASS_IN, ttl);
	resource->rdata_format = RDATA_NAME;
	resource->rdata_parsed.name.name = g_strdup (target);
	resource->rdata = NULL;
	resource->rdlength = 0;
	return resource;
}


GDNSResource*
gdns_resource_new_SRV (const char* name, guint16 priority, guint16 weight,
		guint16 port, const char* target, unsigned long int ttl)
{
	GDNSResource* resource = gdns_resource_new (name, TYPE_SRV, CLASS_IN, ttl);
	resource->rdata_format = RDATA_SRV;
	resource->rdata_parsed.srv.priority = priority;
	resource->rdata_parsed.srv.weight = weight;
	resource->rdata_parsed.srv.port = port;
	resource->rdata_parsed.srv.name = g_strdup (target);
	resource->rdata = NULL;
	resource->rdlength = 0;
	return resource;
}


	/* parse the RR:
                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                                               |
    /                                               /
    /                      NAME                     /
    |                                               |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      TYPE                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                     CLASS                     |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      TTL                      |
    |                                               |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                   RDLENGTH                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--|
    /                     RDATA                     /
    /                                               /
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	*/

GDNSResource* 
gdns_resource_parse (GDNSIBuffer* buf)
{
	GDNSResource* resource;
	int rdata_start;

	resource = g_object_new (gdns_resource_get_type (), NULL);
	resource->name = gdns_ibuffer_get_domain_name (buf);
	resource->type = gdns_ibuffer_get_short (buf);
	resource->klass = gdns_ibuffer_get_short (buf);
	resource->ttl = gdns_ibuffer_get_long (buf);
	resource->rdlength = gdns_ibuffer_get_short (buf);

	rdata_start = gdns_ibuffer_tell (buf);

	resource->expiry = resource->ttl + time (NULL);

	switch (resource->type) {
		case TYPE_A:
			resource->rdata_format = RDATA_A;
			resource->rdata_parsed.a.ip = gdns_ibuffer_get_long (buf);
			break;
		case TYPE_NS:
		case TYPE_CNAME:
		case TYPE_PTR:
			resource->rdata_format = RDATA_NAME;
			resource->rdata_parsed.name.name = 
				gdns_ibuffer_get_domain_name (buf);
			break;
		case TYPE_SRV:
			resource->rdata_format = RDATA_SRV;
			resource->rdata_parsed.srv.priority = 
				gdns_ibuffer_get_short (buf);
			resource->rdata_parsed.srv.weight = 
				gdns_ibuffer_get_short (buf);
			resource->rdata_parsed.srv.port = gdns_ibuffer_get_short (buf);
			resource->rdata_parsed.srv.name = 
				gdns_ibuffer_get_domain_name (buf);
			break;
		default:
			/* for all other types of record the RDATA is presented unparsed */
			resource->rdata_format = RDATA_RAW;
			break;
	}

	gdns_ibuffer_seek (buf, rdata_start);

	resource->rdata = gdns_ibuffer_get_bytes (buf, resource->rdlength);

	return resource;
}

void 
gdns_resource_unparse (GDNSResource* resource, GDNSOBuffer* buf)
{
	int rdlength_offset, rdata_start, rdata_end;
	gdns_obuffer_put_domain_name (buf, resource->name);
	gdns_obuffer_put_short (buf, resource->type);
	gdns_obuffer_put_short (buf, resource->klass);
	gdns_obuffer_put_long (buf, resource->ttl);

	if (resource->rdata_format == RDATA_RAW) {
		/* the RDATA is specified as a length and buffer - easy! */
		gdns_obuffer_put_short (buf, resource->rdlength);
		gdns_obuffer_put_bytes (buf, resource->rdata, resource->rdlength);
		return;
	}

	/* keep track of where we have to put the RDLENGTH */
	rdlength_offset = gdns_obuffer_tell (buf);

	/* put in a fake RDLENGTH */
	gdns_obuffer_put_short (buf, 0);

	/* keep track of where the RDATA starts */
	rdata_start = gdns_obuffer_tell (buf);
	
	switch (resource->rdata_format) {
		case RDATA_A:
			gdns_obuffer_put_long (buf, resource->rdata_parsed.a.ip);
			break;
		case RDATA_NAME:
			gdns_obuffer_put_domain_name (buf, 
					resource->rdata_parsed.name.name);
			break;
		case RDATA_SRV:
			gdns_obuffer_put_short (buf, resource->rdata_parsed.srv.priority);
			gdns_obuffer_put_short (buf, resource->rdata_parsed.srv.weight);
			gdns_obuffer_put_short (buf, resource->rdata_parsed.srv.port);
			gdns_obuffer_put_domain_name (buf, resource->rdata_parsed.srv.name);
			break;
		case RDATA_RAW:
		default:
			g_assert_not_reached ();
			return;
	}

	rdata_end = gdns_obuffer_tell (buf);
	gdns_obuffer_seek (buf, rdlength_offset);
	gdns_obuffer_put_short (buf, rdata_end - rdata_start);
	gdns_obuffer_seek (buf, rdata_end);
}


