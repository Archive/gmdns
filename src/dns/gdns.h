/* gdns.h
 *
 * Copyright (C) 2003-2004 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GDNS_H__
#define __GDNS_H__

#include <glib-object.h>

typedef enum {
	TYPE_A = 1,
	TYPE_NS = 2,
	TYPE_CNAME = 5,
	TYPE_PTR = 12,
	TYPE_TXT = 16,
	TYPE_SRV = 33,

	TYPE_MIN = -1,
	TYPE_INVALID = -1,
	TYPE_MAX = 255
} GDNSType;

typedef enum {
	OPCODE_QUERY = 0,
	OPCODE_IQUERY = 1,
	OPCODE_STATUS = 2,
} GDNSOpcode;

typedef enum {
	RCODE_OK = 0,
	RCODE_FORMAT_ERROR = 1,
	RCODE_SERVER_FAILURE = 2,
	RCODE_NAME_ERROR = 3,
	RCODE_NOT_IMPLEMENTED = 4,
	RCODE_REFUSED = 5,
} GDNSRCode;

typedef enum {
	CLASS_IN = 1, /* Internet */
	CLASS_CS = 2, /* obsolete - CSNET */
	CLASS_CH = 3, /* CHAOS */
	CLASS_HS = 4, /* Hesiod */
	CLASS_ANY = 255, /* Actually this is a QCLASS not CLASS value */
} GDNSClass;

#endif
