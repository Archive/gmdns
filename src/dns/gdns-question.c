#include "gdns-question.h"


/*** GOBJECT MAGIC ***/
static void
gdns_question_finalize (GObject * object)
{
	GDNSQuestion *question = GDNS_QUESTION (object);
	if (question->name) {
		g_free (question->name);
	}
}


static void
gdns_question_class_init (GDNSQuestionClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	/*parent_class = g_type_class_peek_parent (class);*/

	object_class->finalize = gdns_question_finalize;
}


GType
gdns_question_get_type (void)
{
	static GType object_type = 0;

	if (!object_type)
	{
		static const GTypeInfo type_info = {
			sizeof (GDNSQuestionClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gdns_question_class_init,
			(GClassFinalizeFunc) NULL,
			NULL,			/* class data */
			sizeof (GDNSQuestion),
			0,			/* number of pre-allocs */
			(GInstanceInitFunc) NULL,
			NULL			/* value table */
		};

		g_type_init ();

		object_type = g_type_register_static (G_TYPE_OBJECT, 
				"GDNSQuestion", &type_info, 0);
	}

	return object_type;
}


/*** PUBLIC API ***/
GDNSQuestion* 
gdns_question_new (const char* name, 
		   GDNSType type, 
		   GDNSClass klass)
{
	GDNSQuestion* question;

	question = g_object_new (gdns_question_get_type (), NULL);
	question->name = g_strdup (name);
	question->type = type;
	question->klass = klass;

	return question;
}


GDNSQuestion* 
gdns_question_parse (GDNSIBuffer* buf)
{
	GDNSQuestion* question;

	question = g_object_new (gdns_question_get_type (), NULL);
	question->name = gdns_ibuffer_get_domain_name (buf);
	question->type = gdns_ibuffer_get_short (buf);
	question->klass = gdns_ibuffer_get_short (buf);

	return question;
}


void
gdns_question_unparse (GDNSQuestion* question, GDNSOBuffer* buf)
{
	gdns_obuffer_put_domain_name (buf, question->name);
	gdns_obuffer_put_short (buf, question->type);
	gdns_obuffer_put_short (buf, question->klass);
}
