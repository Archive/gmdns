/* vi: sw=4 ts=4
 */

#ifndef __GDNS_MESSAGE_H__
#define __GDNS_MESSAGE_H__

#include <glib-object.h>
#include "gdns.h"
#include "gdns-resource.h"
#include "gdns-question.h"

#define GDNS_TYPE_MESSAGE                        (gdns_message_get_type ())
#define GDNS_MESSAGE(obj)                        (G_TYPE_CHECK_INSTANCE_CAST ((obj), GDNS_TYPE_MESSAGE, GDNSMessage))
#define GDNS_MESSAGE_CLASS(klass)        (G_TYPE_CHECK_CLASS_CAST ((klass), GDNS_TYPE_MESSAGE, GDNSMessageClass))
#define GDNS_IS_MESSAGE(obj)                     (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GDNS_TYPE_MESSAGE))
#define GDNS_IS_MESSAGE_CLASS(klass)     (G_TYPE_CHECK_CLASS_TYPE ((klass), GDNS_TYPE_MESSAGE))
#define GDNS_MESSAGE_GET_CLASS(obj)      (G_TYPE_INSTANCE_GET_CLASS ((obj), GDNS_TYPE_MESSAGE, GDNSMessageClass))
                                                                                
                                                                                
G_BEGIN_DECLS
                                                                                
typedef struct _GDNSMessage GDNSMessage;
typedef struct _GDNSMessageClass GDNSMessageClass;
typedef struct _GDNSMessagePrivate GDNSMessagePrivate;
                                                                                
struct _GDNSMessage
{
	GObject parent;
                                                                                
	guint16 id;
	gboolean qr:1; /* query==FALSE, response==TRUE */
	GDNSOpcode opcode:4;
	gboolean aa:1; /* authorative answer? */
	gboolean tc:1; /* truncated? */
	gboolean rd:1; /* recursion desired? */
	gboolean ra:1; /* recursion available? */
	/*unsigned short z:3;  zero */
	GDNSRCode rcode:4; 
	GList *QDs; /* of GDNSQuestion* */
	GList *ANs; /* of GDNSResource* */
	GList *NSs; /* of GDNSResource* */
	GList *ARs; /* of GDNSResource* */
};

struct _GDNSMessageClass
{
        GObjectClass parent_class;
};

GType gdns_message_get_type (void);

/* Create a new empty GDNSMessage */
GDNSMessage* gdns_message_new (guint16 id, gboolean qr, GDNSOpcode opcode, 
		gboolean aa, gboolean tc, gboolean rd, gboolean ra, GDNSRCode rcode);

/* Create a GDNSMessage object from a message packet */
GDNSMessage* gdns_message_parse (const guint8* buffer, int length);

/* Allocate a buffer and write the message into it */
void gdns_message_unparse (GDNSMessage* message, guint8** buffer, int* length);

/* Add a question */
void gdns_message_add_question (GDNSMessage* message, GDNSQuestion* qs);

/* Add an answer */
void gdns_message_add_answer (GDNSMessage* message, GDNSResource* rr);

/* Add a nameserver */
void gdns_message_add_nameserver (GDNSMessage* message, GDNSResource* rr);

/* Add an additional rr */
void gdns_message_add_additional (GDNSMessage* message, GDNSResource* rr);

G_END_DECLS

#endif
