/* vim:sw=4 ts=4
 */

#include "gdns-message.h"
#include "gdns-ibuffer.h"

/*** GOBJECT MAGIC ***/
static void
gdns_message_finalize (GObject * object)
{
	GDNSMessage *message = GDNS_MESSAGE (object);
	GList* iter;

	for (iter = message->QDs; iter; iter = iter->next) {
		g_object_unref (G_OBJECT (iter->data));
	}
	g_list_free (message->QDs);

	for (iter = message->ANs; iter; iter = iter->next) {
		g_object_unref (G_OBJECT (iter->data));
	}
	g_list_free (message->ANs);

	for (iter = message->NSs; iter; iter = iter->next) {
		g_object_unref (G_OBJECT (iter->data));
	}
	g_list_free (message->NSs);

	for (iter = message->ARs; iter; iter = iter->next) {
		g_object_unref (G_OBJECT (iter->data));
	}
	g_list_free (message->ARs);
}


static void
gdns_message_class_init (GDNSMessageClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	/*parent_class = g_type_class_peek_parent (class);*/

	object_class->finalize = gdns_message_finalize;
}


GType
gdns_message_get_type (void)
{
	static GType object_type = 0;

	if (!object_type)
	{
		static const GTypeInfo type_info = {
			sizeof (GDNSMessageClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gdns_message_class_init,
			(GClassFinalizeFunc) NULL,
			NULL,			/* class data */
			sizeof (GDNSMessage),
			0,			/* number of pre-allocs */
			(GInstanceInitFunc) NULL,
			NULL			/* value table */
		};

		g_type_init ();

		object_type = g_type_register_static (G_TYPE_OBJECT, 
				"GDNSMessage", &type_info, 0);
	}

	return object_type;
}


/*** PUBLIC API ***/
GDNSMessage* 
gdns_message_new (guint16 id, 
				  gboolean qr, 
				  GDNSOpcode opcode, 
				  gboolean aa, 
				  gboolean tc, 
				  gboolean rd, 
				  gboolean ra, 
				  GDNSRCode rcode)
{
	GDNSMessage* message;

	message = g_object_new (gdns_message_get_type (), NULL);
	message->id = id;
	message->qr = qr;
	message->opcode = opcode;
	message->aa = aa;
	message->tc = tc;
	message->rd = rd;
	message->ra = ra;
	message->rcode = rcode;

	return message;
}


void 
gdns_message_add_question (GDNSMessage* message, GDNSQuestion* qs)
{
	g_object_ref (qs);
	message->QDs = g_list_append (message->QDs, qs);
}


void
gdns_message_add_answer (GDNSMessage* message, GDNSResource* rr)
{
	g_object_ref (rr);
	message->ANs = g_list_append (message->ANs, rr);
}


void
gdns_message_add_nameserver (GDNSMessage* message, GDNSResource* rr)
{
	g_object_ref (rr);
	message->NSs = g_list_append (message->NSs, rr);
}


void
gdns_message_add_additional (GDNSMessage* message, GDNSResource* rr)
{
	g_object_ref (rr);
	message->ARs = g_list_append (message->ARs, rr);
}


	/* the header:
                                    1  1  1  1  1  1
      0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                      ID                       |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    QDCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ANCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    NSCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    |                    ARCOUNT                    |
    +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
	*/

GDNSMessage* 
gdns_message_parse (const guint8* bytes, int length)
{
	GDNSMessage* msg;
	guint8 tmp;
	unsigned short qdcount, ancount, nscount, arcount;
	int i;
	GDNSIBuffer* buf = gdns_ibuffer_new (bytes, length);

	msg = g_object_new (gdns_message_get_type (), NULL);

	msg->id = gdns_ibuffer_get_short (buf);

	tmp = gdns_ibuffer_get_byte (buf);
	if(tmp & 0x80) msg->qr = TRUE;
	msg->opcode = (tmp & 0x78) >> 3;
	if(tmp & 0x04) msg->aa = TRUE;
	if(tmp & 0x02) msg->tc = TRUE;
	if(tmp & 0x01) msg->rd = TRUE;

	tmp = gdns_ibuffer_get_byte (buf);
	if(tmp & 0x80) msg->ra = TRUE;
	/* msg->z = (tmp & 0x70) >> 4; */ /* its zero! */
	msg->rcode = tmp & 0x0F;

	qdcount = gdns_ibuffer_get_short (buf);
	ancount = gdns_ibuffer_get_short (buf);
	nscount = gdns_ibuffer_get_short (buf);
	arcount = gdns_ibuffer_get_short (buf);

	/* parse the QDs */
	for (i=0; i<qdcount; i++) {
		gdns_message_add_question (msg, gdns_question_parse (buf));
	}

	/* parse the RRs */
	for (i=0; i<ancount; i++) {
		gdns_message_add_answer (msg, gdns_resource_parse (buf));
	}
	for (i=0; i<nscount; i++) {
		gdns_message_add_nameserver (msg, gdns_resource_parse (buf));
	}
	for (i=0; i<arcount; i++) {
		gdns_message_add_additional (msg, gdns_resource_parse (buf));
	}

	/* FIXME: check that we've consumed the whole packet? */

	gdns_ibuffer_free (buf);

	return msg;
}


void 
gdns_message_unparse (GDNSMessage* message, guint8** buffer, int* length)
{
	GList* iter;
	GDNSOBuffer* buf;
	
	buf = gdns_obuffer_new ();

	gdns_obuffer_put_short (buf, message->id);

	gdns_obuffer_put_byte (buf,
			(message->qr?0x80:0) |
			((message->opcode&0x07) << 3) |
			(message->aa?0x04:0) |
			(message->tc?0x02:0) |
			(message->rd?0x01:0));

	gdns_obuffer_put_byte (buf, (message->ra?0x80:0) | (message->rcode&0x0F));

	gdns_obuffer_put_short (buf, g_list_length (message->QDs));
	gdns_obuffer_put_short (buf, g_list_length (message->ANs));
	gdns_obuffer_put_short (buf, g_list_length (message->NSs));
	gdns_obuffer_put_short (buf, g_list_length (message->ARs));

	for (iter = message->QDs; iter != NULL; iter = iter->next) {
		gdns_question_unparse (GDNS_QUESTION (iter->data), buf);
	}

	for (iter = message->ANs; iter != NULL; iter = iter->next) {
		gdns_resource_unparse (GDNS_RESOURCE (iter->data), buf);
	}

	for (iter = message->NSs; iter != NULL; iter = iter->next) {
		gdns_resource_unparse (GDNS_RESOURCE (iter->data), buf);
	}

	for (iter = message->ARs; iter != NULL; iter = iter->next) {
		gdns_resource_unparse (GDNS_RESOURCE (iter->data), buf);
	}

	gdns_obuffer_get_buffer (buf, buffer, length);

	gdns_obuffer_free (buf);
}
