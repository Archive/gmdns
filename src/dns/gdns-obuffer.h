#ifndef __GDNS_OBUFFER_H__
#define __GDNS_OBUFFER_H__

#include <glib.h>

/*
 * A GDNSOBuffer is a hany data structure that knows how to parse rfc1035 data
 * out of itself.
 */

struct _GDNSOBuffer {
	guint8* bytes; /* the bytes of the buffer */
	int bytes_length; /* the length of the bytes array */
	int data_length; /* the number of bytes that have meaning */
	int off; /* the current position */
};
typedef struct _GDNSOBuffer GDNSOBuffer;

/*
 * Create a new GDNSOBuffer containing the supplied data.
 */
GDNSOBuffer* gdns_obuffer_new ();

/*
 * Discard a buffer and discard the memory associated with it.
 */
void gdns_obuffer_free (GDNSOBuffer* buf);

/*
 * Create a new buffer containing the contents of @buf.
 */
void gdns_obuffer_get_buffer (GDNSOBuffer* buf, guint8** buffer, int* length);

/*
 * Return the current buffer offset.
 */
int gdns_obuffer_tell (GDNSOBuffer* buf);

/*
 * Set the current buffer offset.
 */
void gdns_obuffer_seek (GDNSOBuffer* buf, int off);

/*
 * Append one byte @x to the buffer.
 */
void gdns_obuffer_put_byte (GDNSOBuffer* buf, guint8 x);

/*
 * Append one 16 bit half-word @x to the buffer.
 */
void gdns_obuffer_put_short (GDNSOBuffer* buf, guint16 x);

/*
 * Append one 32 bit word @x to the buffer.
 */
void gdns_obuffer_put_long (GDNSOBuffer* buf, guint32 x);

/*
 * Append @length bytes from @bytes to the buffer.
 */
void gdns_obuffer_put_bytes (GDNSOBuffer* buf, const guint8* bytes, int length);

/*
 * Append a domain name to the buffer.
 */
void gdns_obuffer_put_domain_name (GDNSOBuffer* buf, const char* name);


#endif
