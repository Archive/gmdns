/* vim: ts=4 sw=4
 */
#include <string.h> 

#include "gdns-ibuffer.h"

#define MAX_DOMAIN_NAME 256

GDNSIBuffer* 
gdns_ibuffer_new (const unsigned char* data, int len)
{
	GDNSIBuffer* buf;

	g_assert (data != NULL);
	g_assert (len > 0);

	buf = g_new0 (GDNSIBuffer, 1);
	buf->data = g_malloc (len);
	memcpy (buf->data, data, len);
	buf->len = len;
	buf->off = 0;
	return buf;
}

void 
gdns_ibuffer_free (GDNSIBuffer* buf)
{
	g_assert (buf != NULL);

	g_free (buf->data);
	g_free (buf);
}

/*
 * Seek the buffer to a particular offset.
 */
void
gdns_ibuffer_seek (GDNSIBuffer* buf, int off)
{
	g_assert (buf != NULL);
	g_assert (off <= buf->len && off >= 0);

	buf->off = off;
}

/*
 * Return the current position in the buffer
 */
int
gdns_ibuffer_tell (GDNSIBuffer* buf)
{
	return buf->off;
}

/*
 * Parses out a 16 bit unsigned int in network byteorder and advances the
 * offset pointer 2 bytes.
 */
guint16
gdns_ibuffer_get_short (GDNSIBuffer* buf)
{
	guint16 i = (buf->data[buf->off] << 8) + 
				(buf->data[buf->off+1]);
	buf->off += 2;
	return i;
}


guint32
gdns_ibuffer_get_long (GDNSIBuffer* buf)
{
	guint32 i = (buf->data[buf->off] << 24) + 
				(buf->data[buf->off+1] << 16) +
				(buf->data[buf->off+2] << 8) +
				(buf->data[buf->off+3]);
	buf->off += 4;
	return i;
}


guint8
gdns_ibuffer_peek_byte (GDNSIBuffer* buf)
{
	return buf->data[buf->off];
}


guint8
gdns_ibuffer_get_byte (GDNSIBuffer* buf)
{
	guint8 i = buf->data[buf->off];
	buf->off += 1;
	return i;
}


const unsigned char*
gdns_ibuffer_get_bytes_no_copy (GDNSIBuffer* buf, int length)
{
	buf->off += length;
	return &buf->data[buf->off-length];
}


unsigned char*
gdns_ibuffer_get_bytes (GDNSIBuffer* buffer, int length)
{
	unsigned char* bytes = g_malloc (length);
	memcpy (bytes, gdns_ibuffer_get_bytes_no_copy, length);
	buffer->off += length;
	return bytes;
}


char*
gdns_ibuffer_get_domain_name (GDNSIBuffer* buf)
{
	char* domain_name;
	char* tmp;
	int save_off;
	guint16 next_off;
	guint8 label_len;

	domain_name = g_malloc0 (MAX_DOMAIN_NAME);

	/* FIXME: make sure we don't go beyond MAX_DOMAIN_NAME */

	for (;;) {
		/* check to see if we've got compression for the rest of this
		 * domain name. */
		if (gdns_ibuffer_peek_byte (buf) & 0xC0) {
			/* yep, the lower 14 bits of the next u16 are the offset from the
			 * start of the packet */
			next_off = gdns_ibuffer_get_short (buf) & 0x3FFF;

			/* we need to save our current offset so we can keep going from
			 * the same spot after this */
			save_off = gdns_ibuffer_tell (buf);

			gdns_ibuffer_seek (buf, next_off);
			if (*domain_name) {
				strcat (domain_name, ".");
			}
			tmp = gdns_ibuffer_get_domain_name (buf);
			strcat (domain_name, tmp);
			g_free (tmp);
			gdns_ibuffer_seek (buf, save_off);

			return domain_name;
		}

		/* nope, a boring old label consisting of a length */
		label_len = gdns_ibuffer_get_byte (buf);

		/* if the length is zero we're done */
		if (label_len == 0) {
			return domain_name;
		}

		/* and then some characters */
		if (*domain_name) {
			strcat (domain_name, ".");
		}
		strncat (domain_name, gdns_ibuffer_get_bytes_no_copy (buf, label_len), 
				label_len);
	}
}
