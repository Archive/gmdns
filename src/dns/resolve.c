/* vim: sw=4 ts=4
 */

#include <glib-object.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>

#include "gdns.h"
#include "gdns-message.h"
#include "gdns-question.h"
#include "gdns-resource.h"


static const char*
type_name (GDNSType type)
{
	switch (type) {
		case TYPE_A: return "A"; break;
		case TYPE_NS: return "NS"; break;
		case TYPE_CNAME: return "CNAME"; break;
		case TYPE_PTR: return "PTR"; break;
		case TYPE_TXT: return "TXT"; break;
		case TYPE_SRV: return "SRV"; break;

		default: return "(unknown)"; break;
	}
}


static const char*
class_name (GDNSClass klass)
{
	switch (klass) {
		case CLASS_IN: return "IN"; break;
		case CLASS_CS: return "CS"; break;
		case CLASS_CH: return "CH"; break;
		case CLASS_HS: return "HS"; break;
		case CLASS_ANY: return "ANY"; break;
		default: return "(unknown)"; break;
	}
}


static void
show_resources (GList *list)
{
	GList* iter;
	GDNSResource* resource;

	for (iter=list; iter; iter = iter->next) {
		resource = GDNS_RESOURCE (iter->data);
		g_print ("    name=\"%s\"\n", resource->name);
		g_print ("     type=%s class=%s ttl=%lu\n", type_name (resource->type),
				class_name (resource->klass), resource->ttl);
		g_print ("     rdlength=%u\n", resource->rdlength);
		switch (resource->rdata_format) {
			case RDATA_RAW:
				break;
			case RDATA_A:
				{
					struct in_addr in;
					in.s_addr = resource->rdata_parsed.a.ip;
					g_print ("     ip=%s\n", inet_ntoa (in));
				}
				break;
			case RDATA_NAME:
				g_print ("     name=%s\n", resource->rdata_parsed.name.name);
				break;
			case RDATA_SRV:
				g_print ("     priority=%u weight=%u port=%u\n",
						resource->rdata_parsed.srv.priority,
						resource->rdata_parsed.srv.weight,
						resource->rdata_parsed.srv.port);
				g_print ("     name=%s\n", resource->rdata_parsed.srv.name);
				break;
		}
	}
}


static void
show_message (GDNSMessage* msg)
{
	GList* iter;
	GDNSQuestion* question;

	g_print ("  id = %d\n", msg->id);
	g_print ("  flags =%s%s%s%s%s\n", msg->qr?" QR":"", msg->aa?" AA":"",
			msg->tc?" TC":"", msg->rd?" DD":"", msg->ra?" RA":"");
	g_print ("  opcode = ");
	switch (msg->opcode) {
		case OPCODE_QUERY: g_print ("QUERY\n"); break;
		case OPCODE_IQUERY: g_print ("IQUERY\n"); break;
		case OPCODE_STATUS: g_print ("STATUS\n"); break;
		default: g_print ("(unknown %d)\n", (int)msg->opcode); break;
	}
	g_print ("  rcode = ");
	switch (msg->rcode) {
		case RCODE_OK: g_print ("OK\n"); break;
		case RCODE_FORMAT_ERROR: g_print ("FORMAT_ERROR\n"); break;
		case RCODE_SERVER_FAILURE: g_print ("SERVER_FAILURE\n"); break;
		case RCODE_NAME_ERROR: g_print ("NAME_ERROR\n"); break;
		case RCODE_NOT_IMPLEMENTED: g_print ("NOT_IMPLEMENTED\n"); break;
		case RCODE_REFUSED: g_print ("REFUSED\n"); break;
		default: g_print ("(unknown %d)\n", (int)msg->rcode); break;
	}
	g_print ("  %d questions:\n", g_list_length (msg->QDs));
	for (iter=msg->QDs; iter; iter = iter->next) {
		question = GDNS_QUESTION(iter->data);
		g_print ("    name=\"%s\"\n", question->name);
		g_print ("     type=%s class=%s\n", type_name (question->type),
				class_name (question->klass));
	}
	g_print ("  %d answers:\n", g_list_length (msg->ANs));
	show_resources (msg->ANs);
	g_print ("  %d nameservers:\n", g_list_length (msg->NSs));
	show_resources (msg->NSs);
	g_print ("  %d additional resources:\n", g_list_length (msg->ARs));
	show_resources (msg->ARs);
}


/*
 * resolve <ns> <type1> <name1> [<type2> <name2>...]
 */
int 
main (int argc, char** argv)
{
	GDNSMessage* msg;
	GDNSType type;
	char* type_str;
	struct addrinfo* ns_ai = NULL;
	struct sockaddr_in src_addr = { 0 };
	int sock, err, i;
	guint8* packet = NULL;
	int packet_length = 0;

	/* check the arguments */
	if ((argc < 4) || (argc % 2)) {
		g_print ("%s <nameserver> <type1> <name1> [<type2> <name2>...]\n",
				argv[0]);
		g_print ("  type is one of A, NS, CNAME, PTR, TXT, SRV\n");
		return 1;
	}

	/* FIXME: restrict the search to SOCK_DGRAM, IPPROTO_UDP */
	/* look up the address of the nameserver */
	err = getaddrinfo (argv[1], "domain", NULL, &ns_ai);
	if (err < 0) {
		g_print ("getaddrinfo (\"%s\", \"domain\") failed (%s)\n",
				argv[1], gai_strerror (err));
		return 1;
	}
	if (ns_ai == NULL) {
		g_print ("getaddrinfo (\"%s\", \"domain\") failed\n", argv[1]);
		return 1;
	}

	/* create the socket */
	sock = socket (ns_ai->ai_family, SOCK_DGRAM, IPPROTO_UDP);
	if (sock < 0) {
		perror ("socket");
		return 1;
	}

	/* bind the local end of the socket */
	src_addr.sin_family = AF_INET;
	src_addr.sin_addr.s_addr = htonl (INADDR_ANY);
	src_addr.sin_port = htons (0);
	if (bind (sock, (struct sockaddr*)&src_addr, sizeof (src_addr)) < 0) {
		perror ("bind (INADDR_ANY, 0)");
		return -1;
	}

	/* "connect" the UDP socket */
	if (connect (sock, ns_ai->ai_addr, ns_ai->ai_addrlen) < 0) {
		perror ("connect");
		return -1;
	}

	/* free the addrinfo */
	freeaddrinfo (ns_ai);

	g_type_init ();

	msg = gdns_message_new (1, FALSE, OPCODE_QUERY, FALSE, FALSE, TRUE, FALSE, 
			RCODE_OK);

	for (i = 0; i < (argc-2)/2; i++) {
		type_str = argv[(2*i)+2];
		if (strcmp (type_str, "A") == 0) {
			type = TYPE_A;
		} else if (strcmp (type_str, "NS") == 0) {
			type = TYPE_NS;
		} else if (strcmp (type_str, "CNAME") == 0) {
			type = TYPE_CNAME;
		} else if (strcmp (type_str, "PTR") == 0) {
			type = TYPE_PTR;
		} else if (strcmp (type_str, "TXT") == 0) {
			type = TYPE_TXT;
		} else if (strcmp (type_str, "SRV") == 0) {
			type = TYPE_SRV;
		} else {
			g_print ("unknown type \"%s\". ignoring.\n", type_str);
			continue;
		}
		gdns_message_add_question (msg, 
				gdns_question_new (argv[(2*i)+3], type, CLASS_ANY));
	}

	/* show the query */
	g_print ("query:\n");
	show_message (msg);

	gdns_message_unparse (msg, &packet, &packet_length);

	g_object_unref (msg);

	g_print ("query packet is %d bytes long.\n", packet_length);

	send (sock, packet, packet_length, 0);

	g_free (packet);

	packet = g_malloc0 (512);
	packet_length = recv (sock, packet, 512, 0);

	g_print ("response packet is %d bytes long.\n", packet_length);

	msg = gdns_message_parse (packet, packet_length);

	/* show the response */
	g_print ("response:\n");
	show_message (msg);

	g_object_unref (msg);

	return 0;
}
