/* gmdns-query.h
 *
 * Copyright (C) 2003 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GMDNS_QUERY_H__
#define __GMDNS_QUERY_H__

#include <glib-object.h>
#include <gmdns.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define GMDNS_TYPE_QUERY			(gmdns_query_get_type ())
#define GMDNS_QUERY(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GMDNS_TYPE_QUERY, GmDNSQuery))
#define GMDNS_QUERY_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GMDNS_TYPE_QUERY, GmDNSQueryClass))
#define GMDNS_IS_QUERY(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GMDNS_TYPE_QUERY))
#define GMDNS_IS_QUERY_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GMDNS_TYPE_QUERY))
#define GMDNS_QUERY_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GMDNS_TYPE_QUERY, GmDNSQueryClass))


G_BEGIN_DECLS

typedef struct _GmDNSQuery GmDNSQuery;
typedef struct _GmDNSQueryClass GmDNSQueryClass;
typedef struct _GmDNSQueryPrivate GmDNSQueryPrivate;


/* generic answer */
struct _GmDNSQueryAnswer {
	char *name;
	GmDNSType type;
	unsigned long int ttl;

	char *data;
	size_t length;
};
typedef struct _GmDNSQueryAnswer GmDNSQueryAnswer;
typedef void (*GmDNSQueryAnswerFunc) (GmDNSQuery* query, const GmDNSQueryAnswer *answer);

/* A answer */
struct _GmDNSQueryAAnswer {
	char *name;
	GmDNSType type;
	unsigned long int ttl;

	struct in_addr ip;
};
typedef struct _GmDNSQueryAAnswer GmDNSQueryAAnswer;
typedef void (*GmDNSQueryAAnswerFunc) (GmDNSQuery* query, const GmDNSQueryAAnswer *answer);

/* Named answers */
struct _GmDNSQueryNamedAnswer {
	char *name;
	GmDNSType type;
	unsigned long int ttl;

	char *record_name;
};
typedef struct _GmDNSQueryNamedAnswer GmDNSQueryNamedAnswer;
typedef void (*GmDNSQueryNamedAnswerFunc) (GmDNSQuery* query, const GmDNSQueryNamedAnswer *answer);

/* SRV answer */
struct _GmDNSQuerySRVAnswer {
	char *name;
	GmDNSType type;
	unsigned long int ttl;

	char *record_name;
	unsigned short int priority;
	unsigned short int weight;
	unsigned short int port;
};
typedef struct _GmDNSQuerySRVAnswer GmDNSQuerySRVAnswer;
typedef void (*GmDNSQuerySRVAnswerFunc) (GmDNSQuery* query, const GmDNSQuerySRVAnswer *answer);

/* TXT answer */
struct _GmDNSQueryTXTAnswer {
	char *name;
	GmDNSType type;
	unsigned long int ttl;

	GHashTable *extra;
};
typedef struct _GmDNSQueryTXTAnswer GmDNSQueryTXTAnswer;
typedef void (*GmDNSQueryTXTAnswerFunc) (GmDNSQuery* query, const GmDNSQueryTXTAnswer *answer);


struct _GmDNSQuery
{
	GObject parent;

	/*<private>*/
	GmDNSQueryPrivate *_priv;
};

struct _GmDNSQueryClass
{
	GObjectClass parent_class;

	/* Signals */
	GmDNSQueryAnswerFunc answer;
	GmDNSQueryAAnswerFunc a_answer;
	GmDNSQueryNamedAnswerFunc named_answer;
	GmDNSQuerySRVAnswerFunc srv_answer;
	GmDNSQueryTXTAnswerFunc txt_answer;
};


GType gmdns_query_get_type (void);


GmDNSQuery*	gmdns_query_new		(const char *host, int type);
GmDNSQuery*	gmdns_query_new_A	(const char *host);
GmDNSQuery*	gmdns_query_new_NS	(const char *host);
GmDNSQuery*	gmdns_query_new_CNAME	(const char *host);
GmDNSQuery*	gmdns_query_new_PTR	(const char *host);
GmDNSQuery*	gmdns_query_new_SRV	(const char *host);
GmDNSQuery*	gmdns_query_new_TXT	(const char *host);
void 		gmdns_query_start	(GmDNSQuery* query);
void 		gmdns_query_stop	(GmDNSQuery* query);

G_END_DECLS

#endif /* __GMDNS_QUERY_H__ */
