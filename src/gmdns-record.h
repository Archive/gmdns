/* gmdns-record.h
 *
 * Copyright (C) 2003 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GMDNS_RECORD_H__
#define __GMDNS_RECORD_H__

#include <glib-object.h>

#define GMDNS_TYPE_RECORD			(gmdns_record_get_type ())
#define GMDNS_RECORD(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GMDNS_TYPE_RECORD, GmDNSRecord))
#define GMDNS_RECORD_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GMDNS_TYPE_RECORD, GmDNSRecordClass))
#define GMDNS_IS_RECORD(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GMDNS_TYPE_RECORD))
#define GMDNS_IS_RECORD_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GMDNS_TYPE_RECORD))
#define GMDNS_RECORD_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GMDNS_TYPE_RECORD, GmDNSRecordClass))


G_BEGIN_DECLS

typedef struct _GmDNSRecord GmDNSRecord;
typedef struct _GmDNSRecordClass GmDNSRecordClass;
typedef struct _GmDNSRecordPrivate GmDNSRecordPrivate;


typedef void (*GmDNSRecordConflictFunc) (GmDNSRecord* record, const char* host, int type);


struct _GmDNSRecord
{
	GObject parent;

	/*<private>*/
	GmDNSRecordPrivate *_priv;
};

struct _GmDNSRecordClass
{
	GObjectClass parent_class;

	/* Signals */
	GmDNSRecordConflictFunc conflict;
};


GType gmdns_record_get_type (void);


GmDNSRecord*	gmdns_record_new	(const char *host, int type, 
					 long int ttl, gboolean unique);

void 		gmdns_record_set_raw	(GmDNSRecord* record, 
					 const char* data, int len);
void 		gmdns_record_set_host	(GmDNSRecord* record, const char* host);
void 		gmdns_record_set_ip	(GmDNSRecord* record, 
					 unsigned long int ip);
void 		gmdns_record_set_srv	(GmDNSRecord* record, int priority,
					 int weight, int port,
					 const char* name);
void		gmdns_record_set_txt	(GmDNSRecord* record, 
					 const GHashTable *hash);

G_END_DECLS

#endif /* __GMDNS_RECORD_H__ */
