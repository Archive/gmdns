/* gmdns-service-query.c
 *
 * Copyright (C) 2003 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <string.h>

#include "gmdns-service-query.h"
#include "gmdns-private.h"
#include "gmdns-marshal.h"

enum
{
	PROP_0,
	/* User-settable */
	PROP_NAME,
	PROP_TYPE,
	PROP_DOMAIN,
};

enum
{
	ANSWER,
	LAST_SIGNAL
};


struct _GmDNSServiceQueryPrivate
{
	char *name;
	char *type;
	char *domain;

	GmDNSQuery *ptr_query;

	GHashTable *cache;

	gboolean querying;
};


typedef struct _Service Service;

static gpointer parent_class = NULL;
static gint gmdns_service_query_signals[LAST_SIGNAL] = { 0 };


static void gmdns_service_query_class_init (GmDNSServiceQueryClass * class);
static void gmdns_service_query_instance_init (GmDNSServiceQuery * query);

static void gmdns_service_query_finalize (GObject * object);

static void service_destroy (Service *service);

static void
gmdns_service_query_get_property (GObject * object,
			  guint property,
			  GValue * value,
			  GParamSpec * param_spec)
{
	GmDNSServiceQuery *query = GMDNS_SERVICE_QUERY (object);

	switch (property)
	{
	case PROP_NAME:
		g_value_set_string (value, query->_priv->name);
		break;
	case PROP_TYPE:
		g_value_set_string (value, query->_priv->type);
		break;
	case PROP_DOMAIN:
		g_value_set_string (value, query->_priv->domain);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
		break;
	}
}

static void
gmdns_service_query_set_property (GObject * object,
			  guint property,
			  const GValue * value,
			  GParamSpec * param_spec)
{
	GmDNSServiceQuery *query = GMDNS_SERVICE_QUERY (object);

	switch (property)
	{
	case PROP_NAME:
		gmdns_service_query_set_name (query, 
				g_value_get_string (value));
		break;
	case PROP_TYPE:
		gmdns_service_query_set_type (query, 
				g_value_get_string (value));
		break;
	case PROP_DOMAIN:
		gmdns_service_query_set_domain (query, 
				g_value_get_string (value));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property, param_spec);
		break;
	}
}

GType
gmdns_service_query_get_type (void)
{
	static GType object_type = 0;

	if (!object_type)
	{
		static const GTypeInfo type_info = {
			sizeof (GmDNSServiceQueryClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gmdns_service_query_class_init,
			(GClassFinalizeFunc) NULL,
			NULL,			/* class data */
			sizeof (GmDNSServiceQuery),
			0,			/* number of pre-allocs */
			(GInstanceInitFunc) gmdns_service_query_instance_init,
			NULL			/* value table */
		};

		g_type_init ();

		object_type = g_type_register_static (G_TYPE_OBJECT, 
				"GmDNSServiceQuery", &type_info, 0);
	}

	return object_type;
}


static void
gmdns_service_query_class_init (GmDNSServiceQueryClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->finalize = gmdns_service_query_finalize;
	object_class->get_property = gmdns_service_query_get_property;
	object_class->set_property = gmdns_service_query_set_property;

	gmdns_service_query_signals[ANSWER] = g_signal_new ("answer", 
			G_TYPE_FROM_CLASS (object_class), G_SIGNAL_RUN_FIRST, 
			G_STRUCT_OFFSET (GmDNSServiceQueryClass, answer), 
			NULL, NULL, gmdns_marshal__VOID__POINTER, 
			G_TYPE_NONE, 1, G_TYPE_POINTER);

	g_object_class_install_property (object_class, PROP_NAME, 
			g_param_spec_string ("name", _("Query Name"), 
				_("The service name being queried."), NULL,
				G_PARAM_READWRITE));

	g_object_class_install_property (object_class, PROP_TYPE, 
			g_param_spec_string ("type", _("Query Type"), 
				_("The service type being queried."), "tcp",
				G_PARAM_READWRITE));

	g_object_class_install_property (object_class, PROP_DOMAIN, 
			g_param_spec_string ("domain", _("Query Domain"), 
				_("The domain name being queried."), "local.",
				G_PARAM_READWRITE));
}


static void
gmdns_service_query_instance_init (GmDNSServiceQuery * query)
{
	query->_priv = g_new0 (GmDNSServiceQueryPrivate, 1);
	query->_priv->name = NULL;
	query->_priv->type = g_strdup ("tcp");
	query->_priv->domain = g_strdup ("local.");
	query->_priv->ptr_query = NULL;
	query->_priv->cache = g_hash_table_new_full (g_str_hash, g_str_equal,
			g_free, (GDestroyNotify)service_destroy);
	query->_priv->querying = FALSE;
}


static void
gmdns_service_query_finalize (GObject * object)
{
	GmDNSServiceQuery *query = GMDNS_SERVICE_QUERY (object);
	if (query->_priv->querying) {
		gmdns_service_query_stop (query);
	}
	if (query->_priv->name) {
		g_free (query->_priv->name);
	}
}

/***********************************
 * PRIVATE API                     *
 ***********************************/

struct _Service
{
	GmDNSQuery *a_query;
	GmDNSQuery *srv_query;
	GmDNSQuery *txt_query;
	unsigned long int ttl;
	gboolean informed_application;
	GmDNSServiceQueryAnswer answer;
	/* NOTE: answer.name is owned by the hash table as the key */
};

static void
service_destroy (Service *service)
{
	if (service->a_query) {
		g_object_unref (service->a_query);
		service->a_query = NULL;
	}
	if (service->srv_query) {
		g_object_unref (service->srv_query);
		service->srv_query = NULL;
	}
	if (service->txt_query) {
		g_object_unref (service->txt_query);
		service->txt_query = NULL;
	}
}

static void
service_notify (GmDNSServiceQuery* query, Service *s, gboolean removed)
{
	if (removed) {
		s->answer.answer_type = SERVICE_QUERY_ANSWER_REMOVED;
	} else {
		if (s->informed_application) {
			s->answer.answer_type = SERVICE_QUERY_ANSWER_UPDATED;
		} else {
			s->answer.answer_type = SERVICE_QUERY_ANSWER_FOUND;
			s->informed_application = TRUE;
		}
	}
	g_signal_emit (G_OBJECT (query), gmdns_service_query_signals[ANSWER], 
			0, &s->answer);
}

/**
 * a_callback:
 *
 */

static void 
a_callback (GmDNSQuery* q, 
	    const GmDNSQueryAAnswer* answer, 
	    GmDNSServiceQuery* query) {
	Service *s;

	g_message ("A %s -> %s", answer->name, inet_ntoa (answer->ip));

	s = g_hash_table_lookup (query->_priv->cache, 
			g_object_get_data (G_OBJECT (q), "name"));

	if (s==NULL) {
		g_warning ("unexpected A response");
		/* this callback is unexpected */
		return;
	}

	/* fill in the ip */
	memcpy (&s->answer.ip, &answer->ip, sizeof (s->answer.ip));
	s->answer.valid_fields |= SERVICE_QUERY_ANSWER_IP;
	s->answer.changed_fields = SERVICE_QUERY_ANSWER_IP;
	service_notify (query, s, FALSE);

	/* so we don't want any more A callbacks */
	gmdns_query_stop (s->a_query);
}

/**
 * txt_callback:
 *
 */
static void 
txt_callback (GmDNSQuery* q, 
	      const GmDNSQueryTXTAnswer* answer, 
	      GmDNSServiceQuery* query) {
	Service *s;

	g_message ("TXT %s", answer->name);

	s = g_hash_table_lookup (query->_priv->cache, answer->name);

	if (s==NULL) {
		g_warning ("unexpected TXT response");
		/* this callback is unexpected */
		return;
	}

	g_hash_table_destroy (s->answer.extra);
	s->answer.extra = gmdns_hash_dup (answer->extra);

	/* notify the app */
	s->answer.valid_fields |= SERVICE_QUERY_ANSWER_EXTRA;
	s->answer.changed_fields = SERVICE_QUERY_ANSWER_EXTRA;
	service_notify (query, s, FALSE);

	/* so we don't want any more TXT callbacks */
	gmdns_query_stop (s->txt_query);
}

/**
 * srv_callback:
 *
 */

static void 
srv_callback (GmDNSQuery* q, 
	      const GmDNSQuerySRVAnswer* answer, 
	      GmDNSServiceQuery* query) {
	Service *s;

	g_message ("SRV %s -> port=%d", answer->name, answer->port);

	s = g_hash_table_lookup (query->_priv->cache, answer->name);

	if (s==NULL) {
		g_warning ("unexpected SRV response");
		/* this callback is unexpected */
		return;
	}

	if (s->a_query) {
		g_warning ("a query in progress");
		/* theres already an A in progress */
		return;
	}

	/* fill in the port */
	s->answer.port = answer->port;
	s->answer.valid_fields |= SERVICE_QUERY_ANSWER_PORT;
	s->answer.changed_fields = SERVICE_QUERY_ANSWER_PORT;
	service_notify (query, s, FALSE);

	s->a_query = gmdns_query_new_A (answer->record_name);
	g_signal_connect (s->a_query, "a-answer", (GCallback)a_callback, query);
	/* we need to be able to pass the PTR's name to the A callback so
	 * we can look up the Service structure */
	g_object_set_data_full (G_OBJECT (s->a_query), "name",
			g_strdup (answer->name), g_free);
	gmdns_query_start (s->a_query);
	
	/* so we don't want any more SRV callbacks */
	gmdns_query_stop (s->srv_query);
}
	
/**
 * ptr_callback:
 *
 */

static void 
ptr_callback (GmDNSQuery* q, 
	      const GmDNSQueryNamedAnswer* answer, 
	      GmDNSServiceQuery* query) {

	Service *s;

	g_message ("PTR %s -> %s ttl=%ld", answer->name, answer->record_name, 
			answer->ttl);

	s = g_hash_table_lookup (query->_priv->cache, answer->record_name);

	if (answer->ttl == 0) {
		/* TTL=0 means that we should discard this entry */

		if (s == NULL) {
			/* nobody knows about this service - lets ignore it */
			return;
		}
		if (!s->informed_application) {
			/* the app doesn't know about this yet - we can just
			 * remove it. */
			g_hash_table_remove (query->_priv->cache, 
					answer->record_name);
			return;
		}
		/* inform the app that this service has been removed */
		service_notify (query, s, TRUE);

		/* and remove it from the hash table */
		g_hash_table_remove (query->_priv->cache, answer->record_name);
		return;
	}

	if (s == NULL) {
		s = g_new0 (Service, 1);
		s->informed_application = FALSE;
		s->answer.name = g_strdup (answer->record_name);
		s->answer.extra = g_hash_table_new_full (g_str_hash, 
				g_str_equal, g_free, g_free);
		g_hash_table_insert (query->_priv->cache, s->answer.name, s);
		service_notify (query, s, FALSE);

		s->srv_query = gmdns_query_new_SRV (answer->record_name);
		g_signal_connect (s->srv_query, "srv-answer", 
				(GCallback)srv_callback, query);
		gmdns_query_start (s->srv_query);

		s->txt_query = gmdns_query_new_TXT (answer->record_name);
		g_signal_connect (s->txt_query, "txt-answer", 
				(GCallback)txt_callback, query);
		gmdns_query_start (s->txt_query);
		
	} else {
		/* so, at this point should we start another a/srv query?
		 * I need to work this out. */
	}
	s->ttl = answer->ttl;
}

/***********************************
 * PUBLIC API                      *
 ***********************************/


/**
 * gmdns_service_query_new:
 *
 * Creates a new #GmDNSServiceQuery-struct.
 * @name: the name of the protocol
 *
 * Returns: a new #GmDNSServiceQuery-struct.
 *
 **/
GmDNSServiceQuery *
gmdns_service_query_new (const char *name)
{
	GmDNSServiceQuery *query = 
		(GmDNSServiceQuery*) g_object_new (GMDNS_TYPE_SERVICE_QUERY, 
						   "name", name,
						   NULL);
	return query;
}

/**
 * gmdns_service_query_set name:
 * @query: the #GmDNSServiceQuery
 * @name: the name to set
 *
 * Set the service name for this query. The query must not be running.
 *
 **/
void 
gmdns_service_query_set_name (GmDNSServiceQuery* query, 
			      const char* name)
{
	g_return_if_fail (query != NULL);
	g_return_if_fail (GMDNS_IS_SERVICE_QUERY (query));
	g_return_if_fail (query->_priv->querying == FALSE);
	g_return_if_fail (name != NULL);

	if (query->_priv->name != NULL) {
		g_free (query->_priv->name);
	}
	query->_priv->name = g_strdup (name);
}

/**
 * gmdns_service_query_set name:
 * @query: the #GmDNSServiceQuery
 * @type: the type to set
 *
 * Set the service type for this query. The query must not be running.
 *
 **/
void gmdns_service_query_set_type (GmDNSServiceQuery* query, 
				   const char* type)
{
	g_return_if_fail (query != NULL);
	g_return_if_fail (GMDNS_IS_SERVICE_QUERY (query));
	g_return_if_fail (query->_priv->querying == FALSE);
	g_return_if_fail (type != NULL);

	if (query->_priv->type != NULL) {
		g_free (query->_priv->type);
	}
	query->_priv->type = g_strdup (type);
}

/**
 * gmdns_service_query_set name:
 * @query: the #GmDNSServiceQuery
 * @domain: the name to set
 *
 * Set the domain for this query. The query must not be running.
 *
 **/
void gmdns_service_query_set_domain (GmDNSServiceQuery* query, 
				     const char* domain)
{
	g_return_if_fail (query != NULL);
	g_return_if_fail (GMDNS_IS_SERVICE_QUERY (query));
	g_return_if_fail (query->_priv->querying == FALSE);
	g_return_if_fail (domain != NULL);

	if (query->_priv->domain != NULL) {
		g_free (query->_priv->domain);
	}
	query->_priv->domain = g_strdup (domain);
}

/**
 * gmdns_service_query_start:
 * @query: the #GmDNSServiceQuery
 *
 * Start the query
 *
 **/
void
gmdns_service_query_start (GmDNSServiceQuery * query)
{
	char *name;

	g_return_if_fail (query != NULL);
	g_return_if_fail (GMDNS_IS_SERVICE_QUERY (query));
	g_return_if_fail (query->_priv->querying == FALSE);

	/* set up a PTR query */
	name = g_strdup_printf ("_%s._%s.%s", query->_priv->name,
			query->_priv->type, query->_priv->domain);
	query->_priv->ptr_query = gmdns_query_new_PTR (name);
	g_free (name);
	g_signal_connect (query->_priv->ptr_query, "named-answer", 
			(GCallback)ptr_callback, query);
	gmdns_query_start (query->_priv->ptr_query);
	
	query->_priv->querying = TRUE;
}

/**
 * gmdns_service_query_stop:
 * @query: the #GmDNSServiceQuery
 *
 * Stop the query
 *
 **/
void
gmdns_service_query_stop (GmDNSServiceQuery * query)
{
	g_return_if_fail (query != NULL);
	g_return_if_fail (GMDNS_IS_SERVICE_QUERY (query));
	g_return_if_fail (query->_priv->querying == TRUE);

	if (query->_priv->ptr_query != NULL) {
		gmdns_query_stop (query->_priv->ptr_query);
		query->_priv->ptr_query = NULL;
	}

        query->_priv->querying = FALSE;
}
