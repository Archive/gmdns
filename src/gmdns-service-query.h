/* gmdns-query.h
 *
 * Copyright (C) 2003 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GMDNS_SERVICE_QUERY_H__
#define __GMDNS_SERVICE_QUERY_H__

#include <glib-object.h>
#include <gmdns.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define GMDNS_TYPE_SERVICE_QUERY			(gmdns_service_query_get_type ())
#define GMDNS_SERVICE_QUERY(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GMDNS_TYPE_SERVICE_QUERY, GmDNSServiceQuery))
#define GMDNS_SERVICE_QUERY_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GMDNS_TYPE_SERVICE_QUERY, GmDNSServiceQueryClass))
#define GMDNS_IS_SERVICE_QUERY(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GMDNS_TYPE_SERVICE_QUERY))
#define GMDNS_IS_SERVICE_QUERY_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GMDNS_TYPE_SERVICE_QUERY))
#define GMDNS_SERVICE_QUERY_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GMDNS_TYPE_SERVICE_QUERY, GmDNSServiceQueryClass))


G_BEGIN_DECLS

typedef enum {
	SERVICE_QUERY_ANSWER_NONE  = 0,
	SERVICE_QUERY_ANSWER_PORT  = 1 << 0,
	SERVICE_QUERY_ANSWER_IP    = 1 << 1,
	SERVICE_QUERY_ANSWER_EXTRA = 1 << 2,
} GmDNSServiceQueryAnswerFields;

struct _GmDNSServiceQueryAnswer {
	enum {
		SERVICE_QUERY_ANSWER_FOUND,
		SERVICE_QUERY_ANSWER_UPDATED,
		SERVICE_QUERY_ANSWER_REMOVED
	} answer_type;
	GmDNSServiceQueryAnswerFields valid_fields;
	GmDNSServiceQueryAnswerFields changed_fields;
	char *name;
	unsigned short int port;
	struct in_addr ip;
	GHashTable *extra;
};
typedef struct _GmDNSServiceQueryAnswer GmDNSServiceQueryAnswer;

typedef struct _GmDNSServiceQuery GmDNSServiceQuery;
typedef struct _GmDNSServiceQueryClass GmDNSServiceQueryClass;
typedef struct _GmDNSServiceQueryPrivate GmDNSServiceQueryPrivate;

typedef void (*GmDNSServiceQueryAnswerFunc) (GmDNSServiceQuery* query, const GmDNSServiceQueryAnswer* answer);


struct _GmDNSServiceQuery
{
	GObject parent;

	/*<private>*/
	GmDNSServiceQueryPrivate *_priv;
};

struct _GmDNSServiceQueryClass
{
	GObjectClass parent_class;

	/* Signals */
	GmDNSServiceQueryAnswerFunc answer;
};


GType gmdns_service_query_get_type (void);


GmDNSServiceQuery*	gmdns_service_query_new		(const char* name);
void 			gmdns_service_query_set_name	(GmDNSServiceQuery* query, const char* name);
void 			gmdns_service_query_set_type	(GmDNSServiceQuery* query, const char* type);
void 			gmdns_service_query_set_domain	(GmDNSServiceQuery* query, const char* domain);
void 			gmdns_service_query_start	(GmDNSServiceQuery* query);
void 			gmdns_service_query_stop	(GmDNSServiceQuery* query);

G_END_DECLS

#endif /* __GMDNS_SERVICE_QUERY_H__ */
