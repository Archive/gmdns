/* gmdns.h
 *
 * Copyright (C) 2003 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GMDNS_H__
#define __GMDNS_H__

#include <glib-object.h>

typedef enum {
	TYPE_A = 1,
	TYPE_NS = 2,
	TYPE_CNAME = 5,
	TYPE_PTR = 12,
	TYPE_TXT = 16,
	TYPE_SRV = 33,

	TYPE_MIN = -1,
	TYPE_INVALID = -1,
	TYPE_MAX = 255
} GmDNSType;

gboolean gmdns_init (GMainContext *loop);
void gmdns_shutdown (void);

#endif
