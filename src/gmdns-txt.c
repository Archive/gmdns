/* gmdns-txt.c
 *
 * Copyright (C) 2003 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gmdns-private.h"
#include <glib.h>
#include <string.h>

/**
 * name_string_verify:
 *
 * Check that a name out of a TXT record's name/value pair is
 * valid as per draft-cheshire-dnsext-dns-sd.txt section 6.4
 */
static gboolean
name_string_verify (const char *s, int len)
{
	int i = 0;

	while (s[i] != '\0' && i < len) {
		if (s[i] < 0x20 || s[i] > 0x7E) {
			g_warning ("invalid record (invalid character (0x%x) used in key name)", s[i]);
			return FALSE;
		}
		i++;
	}

	return TRUE;
}

/**
 * gmdns_txt_parse:
 *
 * Parses a TXT record into name/value pairs as described in
 * draft-cheshire-dnsext-dns-sd.txt section 6
 * @data: the contents of the TXT record to parse
 * @len: the length of @data
 *
 * Returns: a newly allocated hash table containing the SRV TXT data or NULL
 * if no data could be parsed. This hash table's keys/values will be freed
 * by g_hash_table_destroy.
 */

GHashTable *
gmdns_txt_parse (const unsigned char* data, size_t len) {
	GHashTable *hash = NULL;
	int i;
	int pairlen;

	for (i=0; i<len;) {
		char *name, *value;
		int namelen, valuelen;
		unsigned char *work;
		const unsigned char *equal;

		pairlen = data[i];
		i++;

		if (pairlen == 0) {
			/* empty pair */
			continue;
		}

		if (pairlen+i > len) {
			g_warning ("invalid record (pair claims to extend beyond the end of the record (%d + %d > len = %d))", pairlen, i, len);
			return hash;
		}

		work = g_strndup (data + i, pairlen);

		equal = strchr (work, '=');
		if (equal == NULL) {
			/* theres no value, just a name, ignoring it */
			g_free (work);
			i += pairlen;
			continue;
		} else {
			namelen = equal - work;
			valuelen = pairlen - namelen -1;
		}

		if (namelen > pairlen) {
			g_free (work);
			g_warning ("invalid record (name claims to extend beyond the end of the pair)");
			return hash;
		}

		if (name_string_verify (work, namelen) == FALSE) {
			g_free (work);
			return hash;
		}

		name = g_strndup (work, namelen);
		value = g_strndup (work + namelen + 1, valuelen);

		if (hash == NULL) {
			hash = gmdns_hash_new ();
		}

		g_hash_table_insert (hash, name, value);

		g_free (work);
		g_free (name);
		g_free (value);

		i += pairlen;
	}

	return hash;

}

struct _txt_build_state {
	int len;
	int off;
	unsigned char *txt;
};

static void
count_size (gpointer k, gpointer v, gpointer d)
{
	struct _txt_build_state *state = (struct _txt_build_state *)d;
	state->len += strlen ((char*)k) + strlen ((char*)v) + 2; /*length,"="*/
}

static void
build_txt (gpointer k, gpointer v, gpointer d)
{
	struct _txt_build_state *state = (struct _txt_build_state *)d;
	unsigned char klen, vlen;

	g_return_if_fail (name_string_verify (k, strlen (k) + 1));

	klen = strlen ((char*)k);
	vlen = strlen ((char*)v);

	state->txt[state->off] = klen + vlen + 1;
	state->off++;
	memcpy (&state->txt[state->off], k, klen);
	state->off += klen;
	state->txt[state->off] = '=';
	state->off++;
	memcpy (&state->txt[state->off], v, vlen);
	state->off += vlen;

	g_assert (state->off <= 1300);
	if (state->off > 200) {
		g_warning ("TXT records shouldn't be more than 200 bytes long");
	}
}


/**
 * gmdns_txt_build:
 *
 * Builds  TXT record from name/value pairs as described in
 * draft-cheshire-dnsext-dns-sd.txt section 6
 * @hash: a hash table of name/value pairs
 * @txt: a pointer to a pointer that will point to the TXT record after the
 * function completes - this should be freed with g_free.
 * @len: a porinter where the length of @txt will be stored
 *
 */
void
gmdns_txt_build (const GHashTable *hash, unsigned char** txt, size_t *len) 
{
	struct _txt_build_state state = { 0, 0, 0 };
	g_hash_table_foreach ((GHashTable*)hash, count_size, &state);
	state.txt = g_malloc0 (state.len);
	g_hash_table_foreach ((GHashTable*)hash, build_txt, &state);
	*txt = state.txt;
	*len = state.len;
}




GHashTable* 
gmdns_hash_new (void)
{
	return g_hash_table_new_full (g_str_hash, g_str_equal, g_free, g_free);
}

static void
copy_values (gpointer k, gpointer v, gpointer d) {
	g_message ("copy_values (%p, %p, %p)", k, v, d);
	g_hash_table_insert ((GHashTable*)d, g_strdup ((char*)k), 
			g_strdup ((char*)v));
}
	
GHashTable* gmdns_hash_dup (const GHashTable *hash) {
	GHashTable *newhash = gmdns_hash_new ();

	g_hash_table_foreach ((GHashTable*)hash, copy_values, newhash);

	return newhash;
}
