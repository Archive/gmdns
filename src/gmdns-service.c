/* gmdns-service.c
 *
 * Copyright (C) 2003 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

/*
 * So, to advertise a service we have to set up the following records:
 *
 * PTR _SERVICE._PROTOCOL.DOMAIN.
 *      -> NAME._SERVICE._PROTOCOL.DOMAIN.
 *
 * SRV NAME._SERVICE._PROTOCOL.DOMAIN.
 *      -> (SERVICE-NAME.DOMAIN., PRIORITY, WEIGHT, PORT)
 *
 * TXT NAME._SERVICE._PROTOCOL.DOMAIN 
 *      -> EXTRA
 *
 * A   SERVICE-NAME.DOMAIN.
 *      -> IP
 *
 */

#include "gmdns-service.h"
#include "gmdns-private.h"
#include "gmdns-record.h"

struct _GmDNSServicePrivate
{
	/* init properties */
	char *name;
	char *service;
	char *protocol;
	char *domain;

	/* service properties */
	GHashTable *extra;
	unsigned short int port;
	struct in_addr ip;
	gboolean extra_set:1;
	gboolean port_set:1;
	gboolean ip_set:1;

	gboolean running:1;
	
	/* DNS records */
	GmDNSRecord *ptr;
	GmDNSRecord *srv;
	GmDNSRecord *a;
	GmDNSRecord *txt;

	/* DNS names */
	char *ptr_name;
	char *srv_name;
	char *a_name;
};


static void gmdns_service_class_init (GmDNSServiceClass * class);
static void gmdns_service_instance_init (GmDNSService * service);

static void gmdns_service_finalize (GObject * object);

GType
gmdns_service_get_type (void)
{
	static GType object_type = 0;

	if (!object_type)
	{
		static const GTypeInfo type_info = {
			sizeof (GmDNSServiceClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gmdns_service_class_init,
			(GClassFinalizeFunc) NULL,
			NULL,			/* class data */
			sizeof (GmDNSService),
			0,			/* number of pre-allocs */
			(GInstanceInitFunc) gmdns_service_instance_init,
			NULL			/* value table */
		};

		g_type_init ();

		object_type = g_type_register_static (G_TYPE_OBJECT, 
				"GmDNSService", &type_info, 0);
	}

	return object_type;
}


static void
gmdns_service_class_init (GmDNSServiceClass * class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);

	object_class->finalize = gmdns_service_finalize;
}


static void
gmdns_service_instance_init (GmDNSService * service)
{
	service->_priv = g_new0 (GmDNSServicePrivate, 1);

	service->_priv->name = NULL;
	service->_priv->service = NULL;
	service->_priv->domain = NULL;
	service->_priv->protocol = NULL;

	service->_priv->extra = NULL;
	service->_priv->port = 0;
	service->_priv->ip.s_addr = 0;
	service->_priv->extra_set = FALSE;
	service->_priv->port_set = FALSE;
	service->_priv->ip_set = FALSE;

	service->_priv->running = FALSE;

	service->_priv->ptr = NULL;
	service->_priv->srv = NULL;
	service->_priv->a = NULL;
	service->_priv->txt = NULL;

	service->_priv->ptr_name = NULL;
	service->_priv->srv_name = NULL;
	service->_priv->a_name = NULL;
}


static void
gmdns_service_finalize (GObject * object)
{
	GmDNSService *service = GMDNS_SERVICE (object);

	if (service->_priv->name != NULL) {
		g_free (service->_priv->name);
	}
	if (service->_priv->service != NULL) {
		g_free (service->_priv->service);
	}
	if (service->_priv->domain != NULL) {
		g_free (service->_priv->domain);
	}
	if (service->_priv->protocol != NULL) {
		g_free (service->_priv->protocol);
	}

	if (service->_priv->extra != NULL) {
		g_hash_table_destroy (service->_priv->extra);
	}

	if (service->_priv->ptr != NULL) {
		g_object_unref (service->_priv->ptr);
	}
	if (service->_priv->srv != NULL) {
		g_object_unref (service->_priv->srv);
	}
	if (service->_priv->a != NULL) {
		g_object_unref (service->_priv->a);
	}
	if (service->_priv->txt != NULL) {
		g_object_unref (service->_priv->txt);
	}

	if (service->_priv->ptr_name != NULL) {
		g_free (service->_priv->ptr_name);
	}
	if (service->_priv->srv_name != NULL) {
		g_free (service->_priv->srv_name);
	}
	if (service->_priv->a_name != NULL) {
		g_free (service->_priv->a_name);
	}

	g_free (service->_priv);
}

/***********************************
 * PRIVATE API                     *
 ***********************************/

static void
stop_PTR (GmDNSService *service)
{
	if (service->_priv->ptr != NULL) {
		g_message ("removing PTR record");
		g_object_unref (service->_priv->ptr);
		service->_priv->ptr = NULL;
	}
}

static void
start_PTR (GmDNSService *service)
{
	g_return_if_fail (service->_priv->ptr_name != NULL);
	g_return_if_fail (service->_priv->srv_name != NULL);

	stop_PTR (service);

	g_message ("creating PTR record");

	/* set the PTR record to point to the SRV record */
	service->_priv->ptr = gmdns_record_new (service->_priv->ptr_name, 
			TYPE_PTR, 120, FALSE);
	gmdns_record_set_host (service->_priv->ptr, service->_priv->srv_name);
}

static void
stop_SRV_A (GmDNSService *service)
{
	if (service->_priv->srv != NULL) {
		g_message ("removing SRV record");
		g_object_unref (service->_priv->srv);
		service->_priv->srv = NULL;
	}
	if (service->_priv->a != NULL) {
		g_message ("removing A record");
		g_object_unref (service->_priv->a);
		service->_priv->a = NULL;
	}
}

static void
start_SRV_A (GmDNSService *service)
{
	g_return_if_fail (service->_priv->port_set);
	g_return_if_fail (service->_priv->ip_set);

	stop_SRV_A (service);

	g_message ("creating SRV and A records");

	/* set up the SRV record with our service info */
	service->_priv->srv = gmdns_record_new (service->_priv->srv_name, 
			TYPE_SRV, 600, TRUE);
	gmdns_record_set_srv (service->_priv->srv, 0, 0, service->_priv->port, 
			service->_priv->a_name);

	/* set up the SRV record with our service info */
	service->_priv->a = gmdns_record_new (service->_priv->a_name, 
			TYPE_A, 600, TRUE);
	gmdns_record_set_ip (service->_priv->a, service->_priv->ip.s_addr);
}

static void
stop_TXT (GmDNSService *service)
{
	if (service->_priv->txt != NULL) {
		g_message ("removing TXT record");
		g_object_unref (service->_priv->txt);
		service->_priv->txt = NULL;
	}
}

static void
start_TXT (GmDNSService *service)
{
	g_return_if_fail (service->_priv->extra_set);
	
	stop_TXT (service);

	g_message ("creating TXT record");

	/* set up the TXT record */
	service->_priv->txt = gmdns_record_new (service->_priv->srv_name, 
			TYPE_TXT, 600, TRUE);
	gmdns_record_set_txt (service->_priv->txt, service->_priv->extra);
}

/***********************************
 * PUBLIC API                      *
 ***********************************/

/**
 * gmdns_service_new:
 *
 * Creates a new, unopened #GmDNSService-struct.
 * @name: the visible name of the serice
 * @service: the service type (eg: "http")
 * @ip: the ip address of the service
 * @port: the port of the service
 *
 * Returns: a new #GmDNSService-struct.
 *
 **/
GmDNSService*
gmdns_service_new (const char* name, const char* service_name, 
		const char* protocol, const char* domain) 
{
	GmDNSService *service;

	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (service_name != NULL, NULL);

	service = 
		(GmDNSService*) g_object_new (GMDNS_TYPE_SERVICE, NULL);

	if (protocol == NULL) {
		protocol = "tcp";
	}

	if (domain == NULL) {
		domain = "local";
	}

	service->_priv->name = g_strdup (name);
	service->_priv->service = g_strdup (service_name);
	service->_priv->protocol = g_strdup (protocol);
	service->_priv->domain = g_strdup (domain);

	/* the DNS name of our PTR record */
	service->_priv->ptr_name = g_strdup_printf ("_%s._%s.%s.", 
			service_name, protocol, domain);
	/* the DNS name of our SRV & TXT records */
	service->_priv->srv_name = g_strdup_printf ("%s._%s._%s.%s.", name, 
			service_name, protocol, domain);
	/* the DNS name of our A record */
	service->_priv->a_name = g_strdup_printf ("%s-%s.%s.", service_name, 
			name, domain);

	return service;
}

void
gmdns_service_start (GmDNSService *service)
{
	g_return_if_fail (service != NULL);
	g_return_if_fail (GMDNS_IS_SERVICE (service));
	g_return_if_fail (!service->_priv->running);
	g_return_if_fail (service->_priv->port_set);
	g_return_if_fail (service->_priv->ip_set);

	start_SRV_A (service);
	if (service->_priv->extra_set) {
		start_TXT (service);
	}
	start_PTR (service);
}

void
gmdns_service_stop (GmDNSService *service)
{
	g_return_if_fail (service != NULL);
	g_return_if_fail (GMDNS_IS_SERVICE (service));
	g_return_if_fail (service->_priv->running);

	stop_PTR (service);
	stop_TXT (service);
	stop_SRV_A (service);
}

void 
gmdns_service_set_address (GmDNSService *service, 
		unsigned long int ip, unsigned short int port)
{
	g_return_if_fail (service != NULL);
	g_return_if_fail (GMDNS_IS_SERVICE (service));


	if (service->_priv->running) {
		stop_SRV_A (service);
	}

	service->_priv->ip.s_addr = ip;
	service->_priv->ip_set = TRUE;
	service->_priv->port = port;
	service->_priv->port_set = TRUE;

	if (service->_priv->running) {
		start_SRV_A (service);
	}
}

void
gmdns_service_set_extra (GmDNSService *service, const GHashTable *extra)
{
	g_return_if_fail (service != NULL);
	g_return_if_fail (GMDNS_IS_SERVICE (service));

	if (service->_priv->running) {
		stop_TXT (service);
	}

	if (service->_priv->extra_set) {
		g_hash_table_destroy (service->_priv->extra);
		service->_priv->extra = NULL;
	}

	if (extra != NULL) {
		service->_priv->extra = gmdns_hash_dup (extra);
		service->_priv->extra_set = TRUE;
		if (service->_priv->running) {
			start_TXT (service);
		}
	} else {
		service->_priv->extra_set = FALSE;
	}
}

void
gmdns_service_add_extra (GmDNSService *service,
		const char *field, const char *value)
{
	g_return_if_fail (service != NULL);
	g_return_if_fail (GMDNS_IS_SERVICE (service));
	g_return_if_fail (field != NULL);
	g_return_if_fail (value != NULL);

	if (service->_priv->running) {
		stop_TXT (service);
	}

	if (!service->_priv->extra_set) {
		service->_priv->extra = gmdns_hash_new ();
	}

	g_hash_table_replace (service->_priv->extra,
			g_strdup (field), g_strdup (value));

	service->_priv->extra_set = TRUE;

	if (service->_priv->running) {
		start_TXT (service);
	}
}

