/* gmdns-private.h
 *
 * Copyright (C) 2003 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GMDNS_PRIVATE_H__
#define __GMDNS_PRIVATE_H__

#include "mdnsd.h"
#include "gmdns-query.h" /* for GmDNSQuery type definition */
#include "gmdns.h" /* for GmDNSType type definition */

/* protecting access to the mdnsd engine */
#define GMDNS_ENTER /* nothing */
#define GMDNS_LEAVE /* nothing */

/* shared data */
struct GmDNSSharedData {
	gboolean initialized:1;
	mdnsd d;

	GMainContext* mainloop;
	
	/* FIXME: support more than one socket/interface */
	int sock;
	GIOChannel* sock_channel;
	GSource* sock_source;

	GSource* timeout_source;

	/* list of currently active queries */
	GList *active_queries;
};
extern struct GmDNSSharedData gmdns_shared_data;

/* accessors for shared data */
#define GMDNS_MDNSD (gmdns_shared_data.d)

/* private functions */
void gmdns_events_pending (void);
void gmdns_aquery_add (GmDNSType type, const char* name, GmDNSQuery *query);
void gmdns_aquery_remove (GmDNSType type, const char* name, GmDNSQuery *query);
void gmdns_query_notify (GmDNSQuery *query, mdnsda a);

GHashTable* gmdns_hash_new (void);
GHashTable* gmdns_hash_dup (const GHashTable *hash);

GHashTable* gmdns_txt_parse (const unsigned char* data, size_t len);
void gmdns_txt_build (const GHashTable *hash, unsigned char** txt, size_t *len);

gboolean gmdns_str_ascii_case_equals (gconstpointer v, gconstpointer v2);

/* i18n support taken from libgtcpconnection's gtcp-i18n.h */
#ifdef ENABLE_NLS
#	include <libintl.h>
#	ifdef HAVE_GETTEXT
#		define _(String) gettext (String)
#	else
#		define _(String) (String)
#	endif /* HAVE_GETTEXT */
#	define N_(String) gettext_noop (String)
#else
/* Stubs that do something close enough.  */
#	define textdomain(String) (String)
#	define gettext(String) (String)
#	define dgettext(Domain,Message) (Message)
#	define dcgettext(Domain,Message,Type) (Message)
#	define bindtextdomain(Domain,Directory) (Domain)
#	define _(String) (String)
#	define N_(String) (String)
#endif

#endif /* __GMDNS_PRIVATE_H__ */
