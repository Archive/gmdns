/* gmdns-service.h
 *
 * Copyright (C) 2003 Ian McKellar
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; version 2.1 of the
 * License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GMDNS_SERVICE_H__
#define __GMDNS_SERVICE_H__

#include <glib-object.h>

#define GMDNS_TYPE_SERVICE			(gmdns_service_get_type ())
#define GMDNS_SERVICE(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GMDNS_TYPE_SERVICE, GmDNSService))
#define GMDNS_SERVICE_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GMDNS_TYPE_SERVICE, GmDNSServiceClass))
#define GMDNS_IS_SERVICE(obj)			(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GMDNS_TYPE_SERVICE))
#define GMDNS_IS_SERVICE_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GMDNS_TYPE_SERVICE))
#define GMDNS_SERVICE_GET_CLASS(obj)	(G_TYPE_INSTANCE_GET_CLASS ((obj), GMDNS_TYPE_SERVICE, GmDNSServiceClass))


G_BEGIN_DECLS

typedef struct _GmDNSService GmDNSService;
typedef struct _GmDNSServiceClass GmDNSServiceClass;
typedef struct _GmDNSServicePrivate GmDNSServicePrivate;


typedef void (*GmDNSServiceConflictFunc) (GmDNSService* service, const char* host, int type);


struct _GmDNSService
{
	GObject parent;

	/*<private>*/
	GmDNSServicePrivate *_priv;
};

struct _GmDNSServiceClass
{
	GObjectClass parent_class;

	/* Signals */
	//GmDNSServiceConflictFunc conflict;
};


GType gmdns_service_get_type (void);

GmDNSService* gmdns_service_new (const char* name, const char* service, 
		const char* protocol, const char* domain);
void gmdns_service_start (GmDNSService *service);
void gmdns_service_stop (GmDNSService *service);
void gmdns_service_set_address (GmDNSService *service,
		unsigned long int ip, unsigned short int port);
void gmdns_service_set_extra (GmDNSService *service, const GHashTable *extra);
void gmdns_service_add_extra (GmDNSService *service,
		const char *field, const char *value);

G_END_DECLS

#endif /* __GMDNS_SERVICE_H__ */
